package ml.antanaks.school.concert.config;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import ml.antanaks.school.concert.entity.User;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@Configuration
@EnableWebMvc
@ComponentScan("ml.antanaks.school.concert")
public class SpringConfig {

    @Bean
    public Gson createGson() {
        Gson gson = new GsonBuilder()
                .setExclusionStrategies(new ExclusionStrategy() {
                    public boolean shouldSkipClass(Class<?> clazz) {
                        return (clazz == User.class);
                    }

                    public boolean shouldSkipField(FieldAttributes f) {
                        return false;
                    }
                })
                .serializeNulls()
                .create();
        return gson;
    }
}
