package ml.antanaks.school.concert.config;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

public class SpringSecurityInit extends AbstractSecurityWebApplicationInitializer {
    //пустой класс, подключающий спринг-секьюрити к спринг-контейнеру  после деплоя
}
