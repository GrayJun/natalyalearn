package ml.antanaks.school.concert.controller;

import com.google.gson.Gson;
import ml.antanaks.school.concert.dto.song.add.AddSongDtoRequest;
import ml.antanaks.school.concert.dto.song.add.SongDto;
import ml.antanaks.school.concert.dto.user.logout.LogoutUserDtoRequest;
import ml.antanaks.school.concert.dto.user.register.RegisterUserDtoRequest;
import ml.antanaks.school.concert.dto.user.register.RegisterUserDtoResponse;
import ml.antanaks.school.concert.entity.Comment;
import ml.antanaks.school.concert.entity.Song;
import ml.antanaks.school.concert.entity.User;
import ml.antanaks.school.concert.exception.ServerException;
import ml.antanaks.school.concert.repository.CommentRepository;
import ml.antanaks.school.concert.repository.SongRepository;
import ml.antanaks.school.concert.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RestController
@RequestMapping("/database")
public class DataBaseController {

    @Autowired
    private UserController userController;

    @Autowired
    private SongController songController;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private SongRepository songRepository;

    @Autowired
    private CommentRepository commentRepository;

    private String tokenUser1;
    private String tokenUser2;
    private String tokenUser3;
    private String tokenUser4;

    private String songTitle1;
    private String songTitle2;
    private String songTitle3;
    private String songTitle4;

    @PostMapping("/fill")
    private ResponseEntity fill() throws ServerException {
        addUsersToDataBase();
        createCommunityUser();
        addSongsToDataBase();
        addSongRatingAndCommentsToDataBase();
        addLikesCommentToDataBase();
        return ResponseEntity.ok("DataBase filled");
    }

    @PostMapping("/clear")
    public String clear() {
        commentRepository.deleteAll();
        songRepository.deleteAll();
        userRepository.deleteAll();
        return "DataBase cleared";
    }


    private void createCommunityUser() {
        userRepository.save(new User(null, null, "Community", null));
    }

    private void addUsersToDataBase() throws ServerException {
        RegisterUserDtoRequest request1 = new RegisterUserDtoRequest("B1",
                "P1", "login1", "password1");
        String jsonRequestUser1 = new Gson().toJson(request1);
        String jsonResponse1 = userController.registerUser(jsonRequestUser1);
        RegisterUserDtoResponse registerUserDtoResponse1 = new Gson().fromJson(jsonResponse1, RegisterUserDtoResponse.class);
        tokenUser1 = registerUserDtoResponse1.getToken();

        RegisterUserDtoRequest request2 = new RegisterUserDtoRequest("B2",
                "P2", "login2", "password2");
        String jsonRequestUser2 = new Gson().toJson(request2);
        String jsonResponse2 = userController.registerUser(jsonRequestUser2);
        RegisterUserDtoResponse registerUserDtoResponse2 = new Gson().fromJson(jsonResponse2, RegisterUserDtoResponse.class);
        tokenUser2 = registerUserDtoResponse2.getToken();

        RegisterUserDtoRequest request3 = new RegisterUserDtoRequest("B3",
                "P3", "login3", "password3");
        String jsonRequestUser3 = new Gson().toJson(request3);
        String jsonResponse3 = userController.registerUser(jsonRequestUser3);
        RegisterUserDtoResponse registerUserDtoResponse3 = new Gson().fromJson(jsonResponse3, RegisterUserDtoResponse.class);
        tokenUser3 = registerUserDtoResponse3.getToken();

        RegisterUserDtoRequest request4 = new RegisterUserDtoRequest("B4",
                "P4", "login4", "password4");
        String jsonRequestUser4 = new Gson().toJson(request4);
        String jsonResponse4 = userController.registerUser(jsonRequestUser4);
        RegisterUserDtoResponse registerUserDtoResponse4 = new Gson().fromJson(jsonResponse4, RegisterUserDtoResponse.class);
        tokenUser4 = registerUserDtoResponse4.getToken();

        LogoutUserDtoRequest request = new LogoutUserDtoRequest(tokenUser3);
        userController.logoutUser(new Gson().toJson(request));
        if (userRepository.findUserByLogin("login3").getToken() == null) {
            tokenUser3 = null;
        }
    }

    private void addSongsToDataBase() {
        songTitle1 = "Песня1";
        Set<String> composer = Stream.of("Композитор11", "Композитор12", "Композитор13").collect(Collectors.toSet());
        Set<String> authorOfWords = Stream.of("Автор11", "Автор12").collect(Collectors.toSet());
        String songArtist = "Исполнитель1";
        String songTiming = "150";

        List<SongDto> songsUser1 = Stream.of(new SongDto(songTitle1, composer, authorOfWords, songArtist, songTiming))
                .collect(Collectors.toList());
        AddSongDtoRequest addSongDtoRequest = new AddSongDtoRequest(songsUser1, tokenUser1);
        String jsonAddSong = new Gson().toJson(addSongDtoRequest);
        songController.addSong(jsonAddSong);

        songTitle2 = "Песня2";
        Set<String> composer2 = Stream.of("Композитор21", "Композитор22", "Композитор23").collect(Collectors.toSet());
        Set<String> authorOfWords2 = Stream.of("Автор21", "Автор22").collect(Collectors.toSet());
        String songArtist2 = "Исполнитель2";
        String songTiming2 = "300";

        List<SongDto> songsUser2 = Stream.of(new SongDto(songTitle2, composer2, authorOfWords2, songArtist2, songTiming2))
                .collect(Collectors.toList());
        AddSongDtoRequest addSongDtoRequest2 = new AddSongDtoRequest(songsUser2, tokenUser2);
        String jsonAddSong2 = new Gson().toJson(addSongDtoRequest2);
        songController.addSong(jsonAddSong2);

        songTitle3 = "Песня3";
        Set<String> composer3 = Stream.of("Композитор31", "Композитор32").collect(Collectors.toSet());
        Set<String> authorOfWords3 = Stream.of("Автор31").collect(Collectors.toSet());
        String songArtist3 = "Исполнитель3";
        String songTiming3 = "3500";

        List<SongDto> songsUser3 = Stream.of(new SongDto(songTitle3, composer3, authorOfWords3, songArtist3, songTiming3))
                .collect(Collectors.toList());

        AddSongDtoRequest addSongDtoRequest3 = new AddSongDtoRequest(songsUser3, tokenUser2);
        String jsonAddSong3 = new Gson().toJson(addSongDtoRequest3);
        songController.addSong(jsonAddSong3);

        songTitle4 = "Песня4";
        Set<String> composer4 = Stream.of("Композитор31", "Композитор32").collect(Collectors.toSet());
        Set<String> authorOfWords4 = Stream.of("Автор41").collect(Collectors.toSet());
        String songArtist4 = "Исполнитель4";
        String songTiming4 = "55";

        List<SongDto> songsUser4 = Stream.of(new SongDto(songTitle4, composer4, authorOfWords4, songArtist4, songTiming4))
                .collect(Collectors.toList());

        AddSongDtoRequest addSongDtoRequest4 = new AddSongDtoRequest(songsUser4, tokenUser4);
        String jsonAddSong4 = new Gson().toJson(addSongDtoRequest4);
        songController.addSong(jsonAddSong4);
    }

    private void addSongRatingAndCommentsToDataBase() {
        Song song1 = songRepository.findSongBySongTitle(songTitle1);
        Map<String, Integer> rating1 = song1.getRating();
        rating1.put("login2", 2);
        rating1.put("login3", 5);
        song1.getComments().add(new Comment("Хорошая1", "login2"));
        song1.getComments().add(new Comment("Клевая1", "login3"));

        Song song2 = songRepository.findSongBySongTitle(songTitle2);
        Map<String, Integer> rating2 = song2.getRating();
        rating2.put("login1", 4);
        rating2.put("login3", 1);
        song2.getComments().add(new Comment("Хорошая2", "login2"));
        song2.getComments().add(new Comment("Клевая2", "login3"));

        Song song4 = songRepository.findSongBySongTitle(songTitle4);
        song4.getComments().add(new Comment("Клевая4", "login1"));

        songRepository.save(song1);
        songRepository.save(song2);
        songRepository.save(song4);

        Song song3 = songRepository.findSongBySongTitle(songTitle3);
        song3.getComments().add(new Comment("Клевая3", "login2"));

        songRepository.save(song3);
    }

    private void addLikesCommentToDataBase() {
        Song song1 = songRepository.findSongBySongTitle(songTitle1);
        Comment comment1 = commentRepository.findCommentBySongTitleAndCreatorLogin(song1.getSongTitle(), "login2");
        comment1.getLikes().add("login3");
        comment1.getLikes().add("login4");

        Song song2 = songRepository.findSongBySongTitle(songTitle2);
        Comment comment21 = commentRepository.findCommentBySongTitleAndCreatorLogin(song2.getSongTitle(), "login2");
        comment21.getLikes().add("login1");
        comment21.getLikes().add("login3");
        Comment comment22 = commentRepository.findCommentBySongTitleAndCreatorLogin(song2.getSongTitle(), "login3");
        comment22.getLikes().add("login2");

        Song song4 = songRepository.findSongBySongTitle(songTitle4);
        Comment comment4 = commentRepository.findCommentBySongTitleAndCreatorLogin(song4.getSongTitle(), "login1");
        comment4.getLikes().add("login2");
        comment4.getLikes().add("login3");

        commentRepository.save(comment1);
        commentRepository.save(comment21);
        commentRepository.save(comment22);
        commentRepository.save(comment4);
    }
}
