package ml.antanaks.school.concert.controller;

import ml.antanaks.school.concert.dto.song.comment.remove.RemoveCommentDtoRequest;
import ml.antanaks.school.concert.dto.song.comment.remove.RemoveCommentDtoResponse;
import ml.antanaks.school.concert.dto.song.remove.RemoveSongDtoRequest;
import ml.antanaks.school.concert.dto.song.remove.RemoveSongDtoResponse;
import ml.antanaks.school.concert.service.SongService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/song")
public class SongController {

    @Autowired
    private SongService songService;

    @PostMapping(value = "/add", produces = "application/json;charset=UTF-8")
    public String addSong(@RequestBody String requestJsonString) {
        return songService.addSong(requestJsonString);
    }

    @PostMapping(value ="/remove", produces = "application/json;charset=UTF-8")
    public RemoveSongDtoResponse removeSong(@RequestBody RemoveSongDtoRequest requestJsonString) {
        return songService.removeSong(requestJsonString);
    }

    @PostMapping(value ="/rating/add", produces = "application/json;charset=UTF-8")
    public String addRating(@RequestBody String requestJsonString) {
        return songService.pushRating(requestJsonString);
    }

    @PostMapping(value ="/rating/edit", produces = "application/json;charset=UTF-8")
    public String editRating(@RequestBody String requestJsonString) {
        return songService.pushRating(requestJsonString);
    }

    @PostMapping(value ="/rating/remove", produces = "application/json;charset=UTF-8")
    public String removeRating(@RequestBody String requestJsonString) {
        return songService.removeRating(requestJsonString);
    }

    @PostMapping(value ="/comment/add", produces = "application/json;charset=UTF-8")
    public String addComment(@RequestBody String requestJsonString) {
        return songService.addComment(requestJsonString);
    }

    @PostMapping(value ="/comment/edit", produces = "application/json;charset=UTF-8")
    public String editComment(@RequestBody String requestJsonString) {
        return songService.editComment(requestJsonString);
    }

    @PostMapping(value ="/comment/remove", produces = "application/json;charset=UTF-8")
    public RemoveCommentDtoResponse removeComment(@RequestBody RemoveCommentDtoRequest requestJsonString) {
        return songService.removeComment(requestJsonString);
    }

    @PostMapping(value ="/comment/like", produces = "application/json;charset=UTF-8")
    public String likeComment(@RequestBody String requestJsonString) {
        return songService.likeComment(requestJsonString);
    }

    @PostMapping(value ="/comment/remove-like", produces = "application/json;charset=UTF-8")
    public String removeLikeComment(@RequestBody String requestJsonString) {
        return songService.removeLikeComment(requestJsonString);
    }

    @GetMapping(value = "/get/songs", produces = "application/json;charset=UTF-8")
    public String getSongs(@RequestBody String requestJsonString) {
        return songService.getSongs(requestJsonString);
    }

    @GetMapping(value ="/get/songs-by-composers", produces = "application/json;charset=UTF-8")
    public String getSongsByComposers(@RequestBody String requestJsonString) {
        return songService.getSongsByComposers(requestJsonString);
    }

    @GetMapping(value ="/get/songs-by-author-of-words", produces = "application/json;charset=UTF-8")
    public String getSongsByAuthorOfWords(@RequestBody String requestJsonString) {
        return songService.getSongsByAuthorOfWords(requestJsonString);
    }

    @GetMapping(value ="/get/songs-by-song-artist", produces = "application/json;charset=UTF-8")
    public String getSongsBySongArtist(@RequestBody String requestJsonString) {
        return songService.getSongsBySongArtist(requestJsonString);
    }

    @GetMapping(value ="/get/concert", produces = "application/json;charset=UTF-8")
    public String getConcert(@RequestBody String requestJsonString) {
        return songService.getConcert(requestJsonString);
    }
}