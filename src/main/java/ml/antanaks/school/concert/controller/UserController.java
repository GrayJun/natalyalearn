package ml.antanaks.school.concert.controller;

import ml.antanaks.school.concert.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping(value ="/register", produces = "application/json;charset=UTF-8")
    public String registerUser(@RequestBody String requestJsonString) {
        return userService.registerUser(requestJsonString);
    }

    @PostMapping(value ="/logout", produces = "application/json;charset=UTF-8")
    public String logoutUser(@RequestBody String requestJsonString) {
        return userService.logoutUser(requestJsonString);
    }

    @PostMapping(value = "login", produces = "application/json;charset=UTF-8")
    public String loginUser(@RequestBody String requestJsonString) {
        return userService.loginUser(requestJsonString);
    }

    @PostMapping(value = "/leave", produces = "application/json;charset=UTF-8")
    public String leaveUser(@RequestBody String requestJsonString) {
        return userService.leaveUser(requestJsonString);
    }
}