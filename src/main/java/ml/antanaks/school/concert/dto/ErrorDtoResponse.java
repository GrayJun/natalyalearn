package ml.antanaks.school.concert.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public abstract class ErrorDtoResponse {

    @JsonProperty("error")
    private String error;

    @JsonProperty("errorMessage")
    private String errorMessage;

    public ErrorDtoResponse() {
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}
