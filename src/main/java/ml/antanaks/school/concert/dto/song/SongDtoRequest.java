package ml.antanaks.school.concert.dto.song;

import com.fasterxml.jackson.annotation.JsonProperty;
import ml.antanaks.school.concert.exception.ServerErrorCode;
import ml.antanaks.school.concert.exception.ServerException;

public abstract class SongDtoRequest {

    @JsonProperty("token")
    private String token;

    public SongDtoRequest() {
    }

    public SongDtoRequest(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }

    public void validate() throws ServerException {
        if (token.length() == 0) {
            throw new ServerException(ServerErrorCode.SERVER_SONG_WRONG_FIELDS);
        }
    }

    public void setToken(String token) {
        this.token = token;
    }
}