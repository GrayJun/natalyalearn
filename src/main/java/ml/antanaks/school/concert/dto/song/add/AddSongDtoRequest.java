package ml.antanaks.school.concert.dto.song.add;

import ml.antanaks.school.concert.exception.ServerErrorCode;
import ml.antanaks.school.concert.exception.ServerException;
import ml.antanaks.school.concert.dto.song.SongDtoRequest;

import java.util.List;

public class AddSongDtoRequest extends SongDtoRequest {

    private List<SongDto> songs;

    public AddSongDtoRequest(List<SongDto> songs, String token) {
        super(token);
        setSongs(songs);
    }

    public List<SongDto> getSongs() {
        return songs;
    }

    public void setSongs(List<SongDto> songs) {
        this.songs = songs;
    }

    @Override
    public void validate() throws ServerException {
        if (!songs.isEmpty() && getToken().length() != 0) {
            for (SongDto song : songs) {
                song.validate();
            }
        } else {
            throw new ServerException(ServerErrorCode.SERVER_SONG_WRONG_FIELDS);
        }
    }
}