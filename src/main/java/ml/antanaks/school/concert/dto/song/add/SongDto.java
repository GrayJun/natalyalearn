package ml.antanaks.school.concert.dto.song.add;

import ml.antanaks.school.concert.exception.ServerErrorCode;
import ml.antanaks.school.concert.exception.ServerException;

import java.util.Set;

public class SongDto {

    private final String songTitle;
    private final Set<String> composer;
    private final Set<String> authorOfWords;
    private final String songArtist;
    private final String songTiming;

    public SongDto(String songTitle, Set<String> composer, Set<String> authorOfWords, String songArtist, String songTiming) {
        this.songTitle = songTitle;
        this.composer = composer;
        this.authorOfWords = authorOfWords;
        this.songArtist = songArtist;
        this.songTiming = songTiming;
    }

    public String getSongTitle() {
        return songTitle;
    }

    public Set<String> getComposer() {
        return composer;
    }

    public Set<String> getAuthorOfWords() {
        return authorOfWords;
    }

    public String getSongArtist() {
        return songArtist;
    }

    public String getSongTiming() {
        return songTiming;
    }

    public void validate() throws ServerException {
        if (songTitle.length() == 0 &&
                composer.isEmpty() &&
                authorOfWords.isEmpty() &&
                songArtist.length() == 0 &&
                songTiming.length() == 0) {
            throw new ServerException(ServerErrorCode.SERVER_SONG_WRONG_FIELDS);
        }
    }
}