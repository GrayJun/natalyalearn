package ml.antanaks.school.concert.dto.song.comment.add;

import ml.antanaks.school.concert.exception.ServerErrorCode;
import ml.antanaks.school.concert.exception.ServerException;
import ml.antanaks.school.concert.dto.song.SongDtoRequest;

public class AddCommentDtoRequest extends SongDtoRequest {

    private final String songTitle;
    private final String text;

    public AddCommentDtoRequest(String songTitle, String text, String token) {
        super(token);
        this.songTitle = songTitle;
        this.text = text;
    }

    public String getSongTitle() {
        return songTitle;
    }

    public String getText() {
        return text;
    }

    @Override
    public void validate() throws ServerException {
        super.validate();
        if (songTitle.length() == 0 || text == null || text.length() == 0) {
            throw new ServerException(ServerErrorCode.SERVER_SONG_WRONG_FIELDS);
        }
    }
}