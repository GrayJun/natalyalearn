package ml.antanaks.school.concert.dto.song.comment.edit;

import ml.antanaks.school.concert.exception.ServerErrorCode;
import ml.antanaks.school.concert.exception.ServerException;
import ml.antanaks.school.concert.dto.song.SongDtoRequest;

public class EditCommentDtoRequest extends SongDtoRequest {

    private final String songTitle;
    private final String newText;

    public EditCommentDtoRequest(String songTitle, String newText, String token) {
        super(token);
        this.songTitle = songTitle;
        this.newText = newText;
    }

    public String getSongTitle() {
        return songTitle;
    }

    public String getNewText() {
        return newText;
    }

    @Override
    public void validate() throws ServerException {
        super.validate();
        if (songTitle.length() == 0 && newText.length() == 0) {
            throw new ServerException(ServerErrorCode.SERVER_SONG_WRONG_FIELDS);
        }
    }
}