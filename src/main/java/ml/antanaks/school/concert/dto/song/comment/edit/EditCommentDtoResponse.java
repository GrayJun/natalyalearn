package ml.antanaks.school.concert.dto.song.comment.edit;

import ml.antanaks.school.concert.dto.ErrorDtoResponse;

public class EditCommentDtoResponse extends ErrorDtoResponse {

    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}