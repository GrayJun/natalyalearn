package ml.antanaks.school.concert.dto.song.comment.like.add;

import ml.antanaks.school.concert.exception.ServerErrorCode;
import ml.antanaks.school.concert.exception.ServerException;
import ml.antanaks.school.concert.dto.song.SongDtoRequest;

public class LikeCommentDtoRequest extends SongDtoRequest {

    private final String songTitle;
    private final String loginForLike;

    public LikeCommentDtoRequest(String songTitle, String loginForLike, String token) {
        super(token);
        this.songTitle = songTitle;
        this.loginForLike = loginForLike;
    }

    public String getSongTitle() {
        return songTitle;
    }

    public String getLoginForLike() {
        return loginForLike;
    }

    @Override
    public void validate() throws ServerException {
        super.validate();
        if (songTitle.length() == 0 && loginForLike.length() == 0) {
            throw new ServerException(ServerErrorCode.SERVER_SONG_WRONG_FIELDS);
        }
    }
}