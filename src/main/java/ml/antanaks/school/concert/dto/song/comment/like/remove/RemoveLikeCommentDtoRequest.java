package ml.antanaks.school.concert.dto.song.comment.like.remove;

import ml.antanaks.school.concert.exception.ServerErrorCode;
import ml.antanaks.school.concert.exception.ServerException;
import ml.antanaks.school.concert.dto.song.SongDtoRequest;

public class RemoveLikeCommentDtoRequest extends SongDtoRequest {

    private final String songTitle;
    private final String loginForRemoveLike;

    public RemoveLikeCommentDtoRequest(String songTitle, String loginForRemoveLike, String token) {
        super(token);
        this.songTitle = songTitle;
        this.loginForRemoveLike = loginForRemoveLike;
    }

    public String getSongTitle() {
        return songTitle;
    }

    public String getLoginForRemoveLike() {
        return loginForRemoveLike;
    }

    @Override
    public void validate() throws ServerException {
        super.validate();
        if (songTitle.length() == 0 && loginForRemoveLike.length() == 0) {
            throw new ServerException(ServerErrorCode.SERVER_SONG_WRONG_FIELDS);
        }
    }
}