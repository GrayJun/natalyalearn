package ml.antanaks.school.concert.dto.song.comment.like.remove;

import ml.antanaks.school.concert.dto.ErrorDtoResponse;

public class RemoveLikeCommentDtoResponse extends ErrorDtoResponse {

    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}