package ml.antanaks.school.concert.dto.song.comment.remove;

import com.fasterxml.jackson.annotation.JsonProperty;
import ml.antanaks.school.concert.exception.ServerErrorCode;
import ml.antanaks.school.concert.exception.ServerException;
import ml.antanaks.school.concert.dto.song.SongDtoRequest;

public class RemoveCommentDtoRequest extends SongDtoRequest {

    @JsonProperty("songTitle")
    private String songTitle;

    public RemoveCommentDtoRequest(String songTitle, String token) {
        super(token);
        this.songTitle = songTitle;
    }

    public RemoveCommentDtoRequest() {
    }

    public void setSongTitle(String songTitle) {
        this.songTitle = songTitle;
    }

    public String getSongTitle() {
        return songTitle;
    }

    @Override
    public void validate() throws ServerException {
        super.validate();
        if (songTitle.length() == 0) {
            throw new ServerException(ServerErrorCode.SERVER_SONG_WRONG_FIELDS);
        }
    }
}