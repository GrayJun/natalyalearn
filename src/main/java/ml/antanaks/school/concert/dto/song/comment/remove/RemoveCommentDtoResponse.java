package ml.antanaks.school.concert.dto.song.comment.remove;

import com.fasterxml.jackson.annotation.JsonProperty;
import ml.antanaks.school.concert.dto.ErrorDtoResponse;

public class RemoveCommentDtoResponse extends ErrorDtoResponse {

    @JsonProperty("message")
    private String message;

    public RemoveCommentDtoResponse() {
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}