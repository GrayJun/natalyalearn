package ml.antanaks.school.concert.dto.song.concert;

import ml.antanaks.school.concert.dto.song.SongDtoRequest;

public class GetConcertDtoRequest extends SongDtoRequest {
    public GetConcertDtoRequest(String token) {
        super(token);
    }
}