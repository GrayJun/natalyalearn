package ml.antanaks.school.concert.dto.song.concert;

import ml.antanaks.school.concert.dto.ErrorDtoResponse;
import ml.antanaks.school.concert.model.Concert;

import java.util.List;

public class GetConcertDtoResponse extends ErrorDtoResponse {

    private List<Concert> concert;

    public List<Concert> getConcert() {
        return concert;
    }

    public void setConcert(List<Concert> concert) {
        this.concert = concert;
    }
}