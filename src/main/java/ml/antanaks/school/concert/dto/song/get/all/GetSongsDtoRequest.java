package ml.antanaks.school.concert.dto.song.get.all;

import ml.antanaks.school.concert.dto.song.SongDtoRequest;

public class GetSongsDtoRequest extends SongDtoRequest {
    public GetSongsDtoRequest(String token) {
        super(token);
    }
}