package ml.antanaks.school.concert.dto.song.get.artist;

import ml.antanaks.school.concert.exception.ServerErrorCode;
import ml.antanaks.school.concert.exception.ServerException;
import ml.antanaks.school.concert.dto.song.SongDtoRequest;

public class GetSongsBySongArtistDtoRequest extends SongDtoRequest {

    private final String songArtist;

    public GetSongsBySongArtistDtoRequest(String songArtist, String token) {
        super(token);
        this.songArtist = songArtist;
    }

    public String getSongArtist() {
        return songArtist;
    }

    @Override
    public void validate() throws ServerException {
        super.validate();
        if (songArtist.length() == 0) {
            throw new ServerException(ServerErrorCode.SERVER_SONG_WRONG_FIELDS);
        }
    }
}