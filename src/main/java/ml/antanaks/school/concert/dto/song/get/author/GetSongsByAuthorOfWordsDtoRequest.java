package ml.antanaks.school.concert.dto.song.get.author;

import ml.antanaks.school.concert.exception.ServerErrorCode;
import ml.antanaks.school.concert.exception.ServerException;
import ml.antanaks.school.concert.dto.song.SongDtoRequest;

import java.util.Set;

public class GetSongsByAuthorOfWordsDtoRequest extends SongDtoRequest {

    private final Set<String> authorOfWords;

    public GetSongsByAuthorOfWordsDtoRequest(Set<String> authorOfWords, String token) {
        super(token);
        this.authorOfWords = authorOfWords;
    }

    public Set<String> getAuthorOfWords() {
        return authorOfWords;
    }

    @Override
    public void validate() throws ServerException {
        super.validate();
        if (authorOfWords.isEmpty()) {
            throw new ServerException(ServerErrorCode.SERVER_SONG_WRONG_FIELDS);
        }
    }
}