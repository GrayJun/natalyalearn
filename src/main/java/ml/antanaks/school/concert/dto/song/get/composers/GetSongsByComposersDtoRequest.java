package ml.antanaks.school.concert.dto.song.get.composers;

import ml.antanaks.school.concert.exception.ServerErrorCode;
import ml.antanaks.school.concert.exception.ServerException;
import ml.antanaks.school.concert.dto.song.SongDtoRequest;

import java.util.Set;

public class GetSongsByComposersDtoRequest extends SongDtoRequest {

    private final Set<String> composers;

    public GetSongsByComposersDtoRequest(Set<String> composers, String token) {
        super(token);
        this.composers = composers;
    }

    public Set<String> getComposers() {
        return composers;
    }

    @Override
    public void validate() throws ServerException {
        super.validate();
        if (composers.isEmpty()) {
            throw new ServerException(ServerErrorCode.SERVER_SONG_WRONG_FIELDS);
        }
    }
}