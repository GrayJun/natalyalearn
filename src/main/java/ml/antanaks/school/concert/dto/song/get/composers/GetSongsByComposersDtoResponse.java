package ml.antanaks.school.concert.dto.song.get.composers;

import ml.antanaks.school.concert.dto.ErrorDtoResponse;
import ml.antanaks.school.concert.entity.Song;

import java.util.List;

public class GetSongsByComposersDtoResponse extends ErrorDtoResponse {

    private List<Song> songs;

    public List<Song> getSongs() {
        return songs;
    }

    public void setSongs(List<Song> songs) {
        this.songs = songs;
    }
}
