package ml.antanaks.school.concert.dto.song.rating.add;

import ml.antanaks.school.concert.exception.ServerErrorCode;
import ml.antanaks.school.concert.exception.ServerException;
import ml.antanaks.school.concert.dto.song.SongDtoRequest;

public class AddRatingDtoRequest extends SongDtoRequest {

    private final String songTitle;
    private final int rating;

    public AddRatingDtoRequest(String songTitle, int rating, String token) {
        super(token);
        this.songTitle = songTitle;
        this.rating = rating;
    }

    public String getSongTitle() {
        return songTitle;
    }

    public int getRating() {
        return rating;
    }

    @Override
    public void validate() throws ServerException {
        super.validate();
        if (songTitle.length() == 0) {
            throw new ServerException(ServerErrorCode.SERVER_SONG_WRONG_FIELDS);
        }
        if (rating >= 6 || rating <= 0) {
            throw new ServerException(ServerErrorCode.SERVER_WRONG_RATING);
        }
    }
}