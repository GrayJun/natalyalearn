package ml.antanaks.school.concert.dto.song.rating.edit;

import ml.antanaks.school.concert.dto.ErrorDtoResponse;

public class EditRatingDtoResponse extends ErrorDtoResponse {

    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}