package ml.antanaks.school.concert.dto.song.rating.remove;

import ml.antanaks.school.concert.exception.ServerErrorCode;
import ml.antanaks.school.concert.exception.ServerException;
import ml.antanaks.school.concert.dto.song.SongDtoRequest;

public class RemoveRatingDtoRequest extends SongDtoRequest {

    private final String songTitle;

    public RemoveRatingDtoRequest(String songTitle, String token) {
        super(token);
        this.songTitle = songTitle;
    }

    public String getSongTitle() {
        return songTitle;
    }

    @Override
    public void validate() throws ServerException {
        super.validate();
        if (songTitle.length() == 0) {
            throw new ServerException(ServerErrorCode.SERVER_SONG_WRONG_FIELDS);
        }
    }
}