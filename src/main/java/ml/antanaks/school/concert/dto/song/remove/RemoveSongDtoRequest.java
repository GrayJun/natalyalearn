package ml.antanaks.school.concert.dto.song.remove;

import com.fasterxml.jackson.annotation.JsonProperty;
import ml.antanaks.school.concert.exception.ServerErrorCode;
import ml.antanaks.school.concert.exception.ServerException;
import ml.antanaks.school.concert.dto.song.SongDtoRequest;

public class RemoveSongDtoRequest extends SongDtoRequest {

    @JsonProperty("songTitle")
    private String songTitle;

    public RemoveSongDtoRequest(String songTitle, String token) {
        super(token);
        setSongTitle(songTitle);
    }

    public RemoveSongDtoRequest(String token) {
        super(token);
    }

    public RemoveSongDtoRequest() {
    }

    public String getSongTitle() {
        return songTitle;
    }

    public void setSongTitle(String songTitle) {
        this.songTitle = songTitle;
    }

    @Override
    public void validate() throws ServerException {
        super.validate();
        if (songTitle.length() == 0) {
            throw new ServerException(ServerErrorCode.SERVER_SONG_WRONG_FIELDS);
        }
    }
}