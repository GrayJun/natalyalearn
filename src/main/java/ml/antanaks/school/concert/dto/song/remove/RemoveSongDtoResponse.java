package ml.antanaks.school.concert.dto.song.remove;

import com.fasterxml.jackson.annotation.JsonProperty;
import ml.antanaks.school.concert.dto.ErrorDtoResponse;

public class RemoveSongDtoResponse extends ErrorDtoResponse {

    @JsonProperty("message")
    private String message;

    public RemoveSongDtoResponse() {
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}