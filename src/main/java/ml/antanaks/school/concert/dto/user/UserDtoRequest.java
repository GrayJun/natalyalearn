package ml.antanaks.school.concert.dto.user;

import ml.antanaks.school.concert.exception.ServerErrorCode;
import ml.antanaks.school.concert.exception.ServerException;

public abstract class UserDtoRequest {

    private String login;
    private String password;

    public UserDtoRequest() {
    }

    public UserDtoRequest(String login, String password) {
        this.login = login;
        this.password = password;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    public void validate() throws ServerException {
        if (login.length() == 0) {
            throw new ServerException(ServerErrorCode.SERVER_USER_WRONG_LOGIN);
        }
        if (password.length() <= 8) {
            throw new ServerException(ServerErrorCode.SERVER_USER_WRONG_PASSWORD);
        }
    }
}