package ml.antanaks.school.concert.dto.user.leave;

import ml.antanaks.school.concert.exception.ServerErrorCode;
import ml.antanaks.school.concert.exception.ServerException;
import ml.antanaks.school.concert.dto.user.UserDtoRequest;

public class LeaveUserDtoRequest extends UserDtoRequest {

    private final String token;

    public LeaveUserDtoRequest(String token) {
        super();
        this.token = token;
    }

    public String getToken() {
        return token;
    }

    @Override
    public void validate() throws ServerException {
        if (token.length() == 0) {
            throw new ServerException(ServerErrorCode.SERVER_USER_WRONG_TOKEN);
        }
    }
}