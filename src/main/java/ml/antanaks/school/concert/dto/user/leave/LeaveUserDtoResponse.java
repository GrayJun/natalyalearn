package ml.antanaks.school.concert.dto.user.leave;

import ml.antanaks.school.concert.dto.ErrorDtoResponse;

public class LeaveUserDtoResponse extends ErrorDtoResponse {

    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
