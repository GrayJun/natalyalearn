package ml.antanaks.school.concert.dto.user.login;

import ml.antanaks.school.concert.dto.user.UserDtoRequest;

public class LoginUserDtoRequest extends UserDtoRequest {

    public LoginUserDtoRequest(String login, String password) {
        super(login, password);
    }

}