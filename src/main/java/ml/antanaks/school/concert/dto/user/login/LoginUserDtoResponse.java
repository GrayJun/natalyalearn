package ml.antanaks.school.concert.dto.user.login;

import ml.antanaks.school.concert.dto.ErrorDtoResponse;

public class LoginUserDtoResponse extends ErrorDtoResponse {

    private String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}