package ml.antanaks.school.concert.dto.user.logout;

import ml.antanaks.school.concert.exception.ServerErrorCode;
import ml.antanaks.school.concert.exception.ServerException;
import ml.antanaks.school.concert.dto.user.UserDtoRequest;

public class LogoutUserDtoRequest extends UserDtoRequest {

    private final String token;

    public LogoutUserDtoRequest(String token) {
        super();
        this.token = token;
    }

    public String getToken() {
        return token;
    }

    @Override
    public void validate() throws ServerException {
        if (token.length() == 0) {
            throw new ServerException(ServerErrorCode.SERVER_USER_WRONG_TOKEN);
        }
    }
}