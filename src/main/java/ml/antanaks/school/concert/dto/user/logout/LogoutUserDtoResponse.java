package ml.antanaks.school.concert.dto.user.logout;

import ml.antanaks.school.concert.dto.ErrorDtoResponse;

public class LogoutUserDtoResponse extends ErrorDtoResponse {

    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}