package ml.antanaks.school.concert.dto.user.register;

import ml.antanaks.school.concert.exception.ServerErrorCode;
import ml.antanaks.school.concert.exception.ServerException;
import ml.antanaks.school.concert.dto.user.UserDtoRequest;

public class RegisterUserDtoRequest extends UserDtoRequest {

    private String firstName;
    private String lastName;

    public RegisterUserDtoRequest(String firstName, String lastName, String login, String password) {
        super(login, password);
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public RegisterUserDtoRequest() {
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    @Override
    public void validate() throws ServerException {
        super.validate();
        if (firstName.length() == 0) {
            throw new ServerException(ServerErrorCode.SERVER_USER_WRONG_FIRSTNAME);
        }
        if (lastName.length() == 0) {
            throw new ServerException(ServerErrorCode.SERVER_USER_WRONG_LASTNAME);
        }
    }
}