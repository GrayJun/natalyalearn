package ml.antanaks.school.concert.dto.user.register;

import ml.antanaks.school.concert.dto.ErrorDtoResponse;

public class RegisterUserDtoResponse extends ErrorDtoResponse {

    private String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}