package ml.antanaks.school.concert.entity;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "comments")
public class Comment {

    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String text;

    @Column
    private String creatorLogin;

    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "likes")
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JoinColumn
    private Set<String> likes;

    public Comment() {
    }

    public Comment(String text, String creatorLogin) {
        this.text = text;
        this.creatorLogin = creatorLogin;
        likes = new HashSet<>();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public Set<String> getLikes() {
        return likes;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Comment comment = (Comment) o;
        return Objects.equals(text, comment.text) && Objects.equals(creatorLogin, comment.creatorLogin) && Objects.equals(likes, comment.likes);
    }

    @Override
    public int hashCode() {
        return Objects.hash(text, creatorLogin, likes);
    }

    @Override
    public String toString() {
        return "Comment{" +
                "text='" + text + '\'' +
                ", likes=" + likes +
                '}';
    }

    public String getCreatorLogin() {
        return creatorLogin;
    }

    public void setCreatorLogin(String creatorLogin) {
        this.creatorLogin = creatorLogin;
    }
}