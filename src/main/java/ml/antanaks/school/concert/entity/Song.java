package ml.antanaks.school.concert.entity;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.util.*;

@Entity
@Table(name = "songs")
public class Song {

    @Id
    @Column(name = "song_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "song_title")
    private String songTitle;

    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "composers")
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JoinColumn
    private Set<String> composer;

    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "author_of_words")
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JoinColumn
    private Set<String> authorOfWords;

    @Column(name = "song_artist")
    private String songArtist;

    @Column(name = "song_timing")
    private int songTiming;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH, CascadeType.ALL})
    private User user;

    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "rating")
    @MapKeyColumn(name = "login")
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JoinColumn
    private Map<String, Integer> rating = new HashMap<>();

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "song_id")
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Set<Comment> comments = new LinkedHashSet<>();

    public Song(String songTitle, Set<String> composer, Set<String> authorOfWords, String songArtist, int songTiming) {
        this.songTitle = songTitle;
        this.composer = composer;
        this.authorOfWords = authorOfWords;
        this.songArtist = songArtist;
        this.songTiming = songTiming;
    }

    public Song() {
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getSongTitle() {
        return songTitle;
    }

    public Set<String> getComposer() {
        return composer;
    }

    public Set<String> getAuthorOfWords() {
        return authorOfWords;
    }

    public String getSongArtist() {
        return songArtist;
    }

    public int getSongTiming() {
        return songTiming;
    }

    public Map<String, Integer> getRating() {
        return rating;
    }

    public double getAverageRating() {
        Integer summ = rating.values().stream().reduce((acc, x) -> acc + x).orElse(0);
        return (double) Math.round((double) summ / rating.size() * 10) / 10;
    }

    @Override
    public String toString() {
        return "Song{" +
                "songTitle='" + songTitle + '\'' +
                ", composer=" + composer +
                ", authorOfWords=" + authorOfWords +
                ", songArtist='" + songArtist + '\'' +
                ", songTiming=" + songTiming +
                ", rating=" + rating +
                ", comments=" + comments +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Song song = (Song) o;
        return songTiming == song.songTiming && Objects.equals(songTitle, song.songTitle) && Objects.equals(composer, song.composer) && Objects.equals(authorOfWords, song.authorOfWords) && Objects.equals(songArtist, song.songArtist) && Objects.equals(user, song.user) && Objects.equals(rating, song.rating) && Objects.equals(comments, song.comments);
    }

    @Override
    public int hashCode() {
        return Objects.hash(songTitle, composer, authorOfWords, songArtist, songTiming, user, rating, comments);
    }

    public Set<Comment> getComments() {
        return comments;
    }
}