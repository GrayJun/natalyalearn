package ml.antanaks.school.concert.exception;

public enum ServerErrorCode {
    SERVER_USER_WRONG_FIRSTNAME("Имя не может быть пустой строкой"),
    SERVER_USER_WRONG_LASTNAME("Фамилия не может быть пустой строкой"),
    SERVER_USER_WRONG_LOGIN("Логин не может быть пустой строкой"),
    SERVER_USER_WRONG_PASSWORD("Пароль не может быть меньше восьми символов"),
    SERVER_USER_WRONG_TOKEN("Поле токен не может быть пустой строкой"),

    DATABASE_EXIST_LOGIN("Логин уже существует"),
    DATABASE_NOT_FOUND("Логин или токен в базе не найден"),
    DATABASE_NOT_FOUND_USER("Пользователь не зарегистрирован"),
    DATABASE_WRONG_PASSWORD("Пароль введен не верно"),
    DATABASE_EXIST_TOKEN("Токен не существует"),
    DATABASE_SONG_NOT_FOUND("Песня в базе не найдена"),
    DATABASE_WRONG_EDIT_RATING("Автор предложения не может изменить свою оценку"),
    DATABASE_WRONG_REMOVE_RATING("Автор предложения не может изменить свою оценку"),
    DATABASE_WRONG_EDIT_COMMENT("Пользователь уже добавил комментарий ранее"),
    DATABASE_COMMENT_NOT_FOUND("Комментарий в базе не найден"),
    DATABASE_WRONG_EDIT_LIKE("Пользователь уже присоединился к комментарию ранее"),
    DATABASE_LIKE_NOT_FOUND("Пользователь ранее не присоединялся к комментарию"),
    DATABASE_WRONG_POOL_SONG("В концерт не добавлено ни одной песни"),
    DATABASE_WRONG_REMOVE_SONG("Только автор предложения может удалить предложенную песню"),

    SERVER_SONG_WRONG_FIELDS("Все поля должны быть заполнены"),
    SERVER_WRONG_RATING("Оценка должна быть от 1 до 5"),

    HIBERNATE_ERROR("Ошибка базы данных"),
    DATABASE_WRONG_LOGIN_PASSWORD("Не верный логин или пароль");

    private final String errorString;

    ServerErrorCode(String errorString) {
        this.errorString = errorString;
    }

    public String getErrorString() {
        return errorString;
    }
}