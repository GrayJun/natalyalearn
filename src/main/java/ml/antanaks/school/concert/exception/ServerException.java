package ml.antanaks.school.concert.exception;

public class ServerException extends Exception {

    private final ServerErrorCode serverErrorCode;

    public ServerException(ServerErrorCode serverErrorCode) {
        this.serverErrorCode = serverErrorCode;
    }

    public ServerErrorCode getErrorCode() {
        return serverErrorCode;
    }
}