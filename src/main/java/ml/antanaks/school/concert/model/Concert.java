package ml.antanaks.school.concert.model;

import ml.antanaks.school.concert.entity.Comment;
import ml.antanaks.school.concert.entity.Song;

import java.util.Set;

public class Concert {

    private final String songTitle;
    private final Set<String> composer;
    private final Set<String> authorOfWords;
    private final String songArtist;

    private final String creatorLogin;
    private final double averageRating;
    private final Set<Comment> comments;

    public Concert(String songTitle, Set<String> composer, Set<String> authorOfWords, String songArtist, String creatorLogin, double averageRating, Set<Comment> comments) {
        this.songTitle = songTitle;
        this.composer = composer;
        this.authorOfWords = authorOfWords;
        this.songArtist = songArtist;
        this.creatorLogin = creatorLogin;
        this.averageRating = averageRating;
        this.comments = comments;
    }

    public Concert(Song song) {
        this(song.getSongTitle(), song.getComposer(), song.getAuthorOfWords(),
                song.getSongArtist(), song.getUser().getLogin(), song.getAverageRating(), song.getComments());
    }

    public String getSongTitle() {
        return songTitle;
    }

    public double getAverageRating() {
        return averageRating;
    }
}
