package ml.antanaks.school.concert.repository;

import ml.antanaks.school.concert.entity.Comment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface CommentRepository extends JpaRepository<Comment, Long> {

    @Query("select coment from Song s inner join s.comments coment where s.songTitle = :songTitle AND coment.creatorLogin= :creatorLogin")
    Comment findCommentBySongTitleAndCreatorLogin(@Param("songTitle") String songTitle, @Param("creatorLogin") String creatorLogin);
}
