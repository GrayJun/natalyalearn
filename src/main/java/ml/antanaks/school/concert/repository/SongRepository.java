package ml.antanaks.school.concert.repository;

import ml.antanaks.school.concert.entity.Comment;
import ml.antanaks.school.concert.entity.Song;
import ml.antanaks.school.concert.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

@Repository
public interface SongRepository extends JpaRepository<Song, Long> {

    @Query("select s from Song s where s.songTitle = :songTitle")
    Song findSongBySongTitle(@Param("songTitle") String songTitle);

    @Query("select s from Song s where s.user= :user AND s.songTitle = :songTitle")
    Song findSongBySongTitleAndUser(@Param("user") User user, @Param("songTitle") String songTitle);

    @Query("select distinct s " +
            "from Song s left join s.composer c " +
            "where c in (:composers)")
    List<Song> findSongsByComposers(@Param("composers") Set<String> composers);

    @Query("select distinct s " +
            "from Song s left join s.authorOfWords a " +
            "where a in (:authorOfWords)")
    List<Song> findSongsByAuthorOfWords(@Param("authorOfWords") Set<String> authorOfWords);

    @Query("from Song s where s.songArtist = :songArtist")
    List<Song> findSongBySongArtist(@Param("songArtist") String songArtist);
}
