package ml.antanaks.school.concert.repository;

import ml.antanaks.school.concert.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    @Query("select u from User u where u.login = :login")
    User findUserByLogin(@Param("login") String login);

    @Query("select u from User u where u.token = :token")
    User findUserByToken(@Param("token") String token);
}