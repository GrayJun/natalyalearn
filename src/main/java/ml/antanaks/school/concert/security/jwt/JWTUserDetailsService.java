package ml.antanaks.school.concert.security.jwt;

import ml.antanaks.school.concert.entity.User;
import ml.antanaks.school.concert.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class JWTUserDetailsService implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    /**
     * Переопределенный метод для поиска юзера по логину для секьюрити
     **/
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findUserByLogin(username);// ищем юзера по логину
        if (user == null) {
            throw new UsernameNotFoundException("User with username: " + username + " not found");
        }
        UserDetails jwtUser = new JWTUser(
                user.getId(),
                user.getLogin(),
                user.getFirstName(),
                user.getLastName(),
                user.getPassword(),
                true
        );//создаем jwtusera, на основе "модели" юзера, так как спринг работает только с реализациями UserDetails
        return jwtUser;
    }
}
