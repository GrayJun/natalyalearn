package ml.antanaks.school.concert.security.jwt;

import ml.antanaks.school.concert.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

import static org.springframework.util.StringUtils.hasText;

/**
 * Фильтр
 **/
@Component
public class JwtTokenFilter extends GenericFilterBean {

    private static final String AUTHORIZATION = "Authorization";
    private static final String BEARER = "Bearer_";

    @Autowired
    private JwtTokenProvider jwtTokenProvider;

    @Autowired
    private JWTUserDetailsService customUserDetailsService;

    @Autowired
    private UserRepository userRepository;

    /**
     * В этот метод попадают все запросы.
     **/
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        logger.info("do filter...");
        String token = getTokenFromRequest((HttpServletRequest) servletRequest); //из тела запроса извлекаем токен для последующей проверки токена на валидность
        if (token != null && jwtTokenProvider.validateToken(token)) { //если токен НЕ null и он валидный
            if (userRepository.findUserByToken(token) == null) {
                throw new UsernameNotFoundException("Данный токен не верный или не актуальный");
            }
            String userLogin = jwtTokenProvider.getLoginFromToken(token); //получаем логин юзера
            UserDetails customUserDetails = customUserDetailsService.loadUserByUsername(userLogin); //ищем юзера по логину
            UsernamePasswordAuthenticationToken auth = new UsernamePasswordAuthenticationToken(customUserDetails, null, null);
            SecurityContextHolder.getContext().setAuthentication(auth); //все норм! выполняем аутентификацию данного пользователя
        }
        filterChain.doFilter(servletRequest, servletResponse); //сигнализируем спринг секюрити о том что можно дальше продолжать работу цепочке фильтров. передаем запрос дальше фильтрам
    }

    /**
     * Метод для получения токена и проверки на то что он там есть
     **/
    private String getTokenFromRequest(HttpServletRequest request) {
        String bearer = request.getHeader(AUTHORIZATION); //по заголовку(header) получаем значение токена
        if (hasText(bearer) && bearer.startsWith(BEARER)) { //проверка значения на содержание токена. 1-ая проверка на то что содержит только текст, и то что значение начинается с Bearer_
            return bearer.substring(7);
        }
        return null;
    }
}
