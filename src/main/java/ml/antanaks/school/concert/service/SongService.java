package ml.antanaks.school.concert.service;

import com.google.gson.Gson;
import ml.antanaks.school.concert.dto.song.add.AddSongDtoRequest;
import ml.antanaks.school.concert.dto.song.add.AddSongDtoResponse;
import ml.antanaks.school.concert.dto.song.add.SongDto;
import ml.antanaks.school.concert.dto.song.comment.add.AddCommentDtoRequest;
import ml.antanaks.school.concert.dto.song.comment.add.AddCommentDtoResponse;
import ml.antanaks.school.concert.dto.song.comment.edit.EditCommentDtoRequest;
import ml.antanaks.school.concert.dto.song.comment.edit.EditCommentDtoResponse;
import ml.antanaks.school.concert.dto.song.comment.like.add.LikeCommentDtoRequest;
import ml.antanaks.school.concert.dto.song.comment.like.add.LikeCommentDtoResponse;
import ml.antanaks.school.concert.dto.song.comment.like.remove.RemoveLikeCommentDtoRequest;
import ml.antanaks.school.concert.dto.song.comment.like.remove.RemoveLikeCommentDtoResponse;
import ml.antanaks.school.concert.dto.song.comment.remove.RemoveCommentDtoRequest;
import ml.antanaks.school.concert.dto.song.comment.remove.RemoveCommentDtoResponse;
import ml.antanaks.school.concert.dto.song.concert.GetConcertDtoRequest;
import ml.antanaks.school.concert.dto.song.concert.GetConcertDtoResponse;
import ml.antanaks.school.concert.dto.song.get.all.GetSongsDtoRequest;
import ml.antanaks.school.concert.dto.song.get.all.GetSongsDtoResponse;
import ml.antanaks.school.concert.dto.song.get.artist.GetSongsBySongArtistDtoRequest;
import ml.antanaks.school.concert.dto.song.get.artist.GetSongsBySongArtistDtoResponse;
import ml.antanaks.school.concert.dto.song.get.author.GetSongsByAuthorOfWordsDtoRequest;
import ml.antanaks.school.concert.dto.song.get.author.GetSongsByAuthorOfWordsDtoResponse;
import ml.antanaks.school.concert.dto.song.get.composers.GetSongsByComposersDtoRequest;
import ml.antanaks.school.concert.dto.song.get.composers.GetSongsByComposersDtoResponse;
import ml.antanaks.school.concert.dto.song.rating.add.AddRatingDtoRequest;
import ml.antanaks.school.concert.dto.song.rating.add.AddRatingDtoResponse;
import ml.antanaks.school.concert.dto.song.rating.remove.RemoveRatingDtoRequest;
import ml.antanaks.school.concert.dto.song.rating.remove.RemoveRatingDtoResponse;
import ml.antanaks.school.concert.dto.song.remove.RemoveSongDtoRequest;
import ml.antanaks.school.concert.dto.song.remove.RemoveSongDtoResponse;
import ml.antanaks.school.concert.entity.Comment;
import ml.antanaks.school.concert.entity.Song;
import ml.antanaks.school.concert.entity.User;
import ml.antanaks.school.concert.exception.ServerErrorCode;
import ml.antanaks.school.concert.exception.ServerException;
import ml.antanaks.school.concert.model.Concert;
import ml.antanaks.school.concert.repository.CommentRepository;
import ml.antanaks.school.concert.repository.SongRepository;
import ml.antanaks.school.concert.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class SongService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private SongRepository songRepository;

    @Autowired
    private CommentRepository commentRepository;

    @Autowired
    private Gson gson;

    public String addSong(String requestJsonString) {
        AddSongDtoRequest request = new Gson().fromJson(requestJsonString, AddSongDtoRequest.class);
        AddSongDtoResponse response = new AddSongDtoResponse();
        try {
            request.validate();
            User user = userRepository.findUserByToken(request.getToken());
            if (user == null) {
                throw new ServerException(ServerErrorCode.DATABASE_NOT_FOUND);
            }
            for (SongDto songDto : request.getSongs()) {
                Song song = new Song(songDto.getSongTitle(),
                        songDto.getComposer(),
                        songDto.getAuthorOfWords(),
                        songDto.getSongArtist(),
                        Integer.parseInt(songDto.getSongTiming()));
                song.setUser(user);
                song.getRating().put(user.getLogin(), 5);
                user.getSongs().add(song);
                userRepository.save(user);
            }
            response.setMessage("Успешно добавлено в программу концерта");
        } catch (ServerException e) {
            response.setError(e.getErrorCode().toString());
            response.setErrorMessage(e.getErrorCode().getErrorString());
        }
        return new Gson().toJson(response);
    }

    public RemoveSongDtoResponse removeSong(RemoveSongDtoRequest request) {
        RemoveSongDtoResponse response = new RemoveSongDtoResponse();
        try {
            request.validate();

            Song song = songRepository.findSongBySongTitle(request.getSongTitle());
            User user = userRepository.findUserByToken(request.getToken());
            checkIsTrueSongAndUser(song, user);

            User community = userRepository.findUserByLogin("Community");
            Song songForRemove = songRepository.findSongBySongTitleAndUser(user, song.getSongTitle());
            if (songForRemove == null) {
                throw new ServerException(ServerErrorCode.DATABASE_WRONG_REMOVE_SONG);
            }
            if (songForRemove.getRating().size() > 1) {
                songForRemove.setUser(community);
                songRepository.save(songForRemove);
            } else {
                songRepository.delete(songForRemove);
            }

            response.setMessage("Успешно удалена");
        } catch (ServerException e) {
            response.setError(e.getErrorCode().toString());
            response.setErrorMessage(e.getErrorCode().getErrorString());
        }
        return response;
    }


    public String pushRating(String requestJsonString) {
        AddRatingDtoRequest request = new Gson().fromJson(requestJsonString, AddRatingDtoRequest.class);
        AddRatingDtoResponse response = new AddRatingDtoResponse();
        try {
            request.validate();

            Song song = songRepository.findSongBySongTitle(request.getSongTitle());
            User user = userRepository.findUserByToken(request.getToken());
            checkIsTrueSongAndUser(song, user);

            if (song.getUser().getLogin().equals(user.getLogin())) {
                throw new ServerException(ServerErrorCode.DATABASE_WRONG_EDIT_RATING);
            }

            song.getRating().put(user.getLogin(), request.getRating());
            songRepository.save(song);

            response.setMessage("Успешно добавлено");
        } catch (ServerException e) {
            response.setError(e.getErrorCode().toString());
            response.setErrorMessage(e.getErrorCode().getErrorString());
        }
        return new Gson().toJson(response);
    }

    public String removeRating(String requestJsonString) {
        RemoveRatingDtoRequest request = new Gson().fromJson(requestJsonString, RemoveRatingDtoRequest.class);
        RemoveRatingDtoResponse response = new RemoveRatingDtoResponse();
        try {
            request.validate();

            Song song = songRepository.findSongBySongTitle(request.getSongTitle());
            User user = userRepository.findUserByToken(request.getToken());
            checkIsTrueSongAndUser(song, user);

            if (song.getUser().getLogin().equals(user.getLogin())) {
                throw new ServerException(ServerErrorCode.DATABASE_WRONG_REMOVE_RATING);
            }

            song.getRating().remove(user.getLogin());
            songRepository.save(song);

            response.setMessage("Успешно удалено");
        } catch (ServerException e) {
            response.setError(e.getErrorCode().toString());
            response.setErrorMessage(e.getErrorCode().getErrorString());
        }
        return new Gson().toJson(response);
    }

    public String addComment(String requestJsonString) {
        AddCommentDtoRequest request = new Gson().fromJson(requestJsonString, AddCommentDtoRequest.class);
        AddCommentDtoResponse response = new AddCommentDtoResponse();
        try {
            request.validate();

            Song song = songRepository.findSongBySongTitle(request.getSongTitle());
            User user = userRepository.findUserByToken(request.getToken());
            checkIsTrueSongAndUser(song, user);

            if (song.getComments().stream().anyMatch(c -> c.getCreatorLogin().equals(user.getLogin()))) {
                throw new ServerException(ServerErrorCode.DATABASE_WRONG_EDIT_COMMENT);
            }

            Comment comment = new Comment(request.getText(), user.getLogin());
            song.getComments().add(comment);
            songRepository.save(song);

            response.setMessage("Успешно добавлен");
        } catch (ServerException e) {
            response.setError(e.getErrorCode().toString());
            response.setErrorMessage(e.getErrorCode().getErrorString());
        }
        return new Gson().toJson(response);
    }

    public String editComment(String requestJsonString) {
        EditCommentDtoRequest request = new Gson().fromJson(requestJsonString, EditCommentDtoRequest.class);
        EditCommentDtoResponse response = new EditCommentDtoResponse();
        try {
            request.validate();

            Song song = songRepository.findSongBySongTitle(request.getSongTitle());
            User user = userRepository.findUserByToken(request.getToken());
            Comment commentEdit = commentRepository.findCommentBySongTitleAndCreatorLogin(song.getSongTitle(), user.getLogin());
            checkIsTrueSongAndUserAndComment(song, user, commentEdit);
            User community = userRepository.findUserByLogin("Community");

            if (commentEdit.getLikes().size() > 1) {
                song.getComments().stream()
                        .filter(comment -> Objects.equals(comment, commentEdit))
                        .findFirst()
                        .get().setCreatorLogin(community.getLogin());
                songRepository.save(song);
            } else {
                song.getComments().remove(commentEdit);
                songRepository.save(song);
            }
            Comment comment = new Comment(request.getNewText(), user.getLogin());
            song.getComments().add(comment);
            songRepository.save(song);

            response.setMessage("Успешно изменение добавлено");
        } catch (ServerException e) {
            response.setError(e.getErrorCode().toString());
            response.setErrorMessage(e.getErrorCode().getErrorString());
        }
        return new Gson().toJson(response);
    }

    public RemoveCommentDtoResponse removeComment(RemoveCommentDtoRequest request) {
        RemoveCommentDtoResponse response = new RemoveCommentDtoResponse();
        try {
            request.validate();

            Song song = songRepository.findSongBySongTitle(request.getSongTitle());
            User user = userRepository.findUserByToken(request.getToken());
            Comment commentRemove = commentRepository.findCommentBySongTitleAndCreatorLogin(song.getSongTitle(), user.getLogin());
            checkIsTrueSongAndUserAndComment(song, user, commentRemove);
            User community = userRepository.findUserByLogin("Community");

            if (commentRemove.getLikes().size() > 1) {
                commentRemove.setCreatorLogin(community.getLogin());
                commentRepository.save(commentRemove);
            } else {
                commentRepository.delete(commentRemove);
            }

            response.setMessage("Успешно добавлено");
        } catch (ServerException e) {
            response.setError(e.getErrorCode().toString());
            response.setErrorMessage(e.getErrorCode().getErrorString());
        }
        return response;
    }

    public String likeComment(String requestJsonString) {
        LikeCommentDtoRequest request = new Gson().fromJson(requestJsonString, LikeCommentDtoRequest.class);
        LikeCommentDtoResponse response = new LikeCommentDtoResponse();
        try {
            request.validate();

            Song song = songRepository.findSongBySongTitle(request.getSongTitle());
            User userLiker = userRepository.findUserByToken(request.getToken());
            Comment commentForLike = commentRepository.findCommentBySongTitleAndCreatorLogin(song.getSongTitle(), request.getLoginForLike());
            checkIsTrueSongAndUserAndComment(song, userLiker, commentForLike);

            commentForLike.getLikes().add(userLiker.getLogin());
            commentRepository.save(commentForLike);

            response.setMessage("Успешно добавлен");
        } catch (ServerException e) {
            response.setError(e.getErrorCode().toString());
            response.setErrorMessage(e.getErrorCode().getErrorString());
        }
        return new Gson().toJson(response);
    }

    public String removeLikeComment(String requestJsonString) {
        RemoveLikeCommentDtoRequest request = new Gson().fromJson(requestJsonString, RemoveLikeCommentDtoRequest.class);
        RemoveLikeCommentDtoResponse response = new RemoveLikeCommentDtoResponse();
        try {
            request.validate();

            Song song = songRepository.findSongBySongTitle(request.getSongTitle());
            User userLiker = userRepository.findUserByToken(request.getToken());
            Comment commentForRemoveLike = commentRepository.findCommentBySongTitleAndCreatorLogin(song.getSongTitle(), request.getLoginForRemoveLike());
            checkIsTrueSongAndUserAndComment(song, userLiker, commentForRemoveLike);

            if (commentForRemoveLike.getLikes().contains(userLiker.getLogin())) {
                commentForRemoveLike.getLikes().remove(userLiker.getLogin());
                commentRepository.save(commentForRemoveLike);
            } else {
                throw new ServerException(ServerErrorCode.DATABASE_WRONG_EDIT_LIKE);
            }

            response.setMessage("Изменение успешно добавлено");
        } catch (ServerException e) {
            response.setError(e.getErrorCode().toString());
            response.setErrorMessage(e.getErrorCode().getErrorString());
        }
        return new Gson().toJson(response);
    }

    public String getSongs(String requestJsonString) {
        GetSongsDtoRequest request = new Gson().fromJson(requestJsonString, GetSongsDtoRequest.class);
        GetSongsDtoResponse response = new GetSongsDtoResponse();
        try {
            request.validate();

            List<Song> songs = songRepository.findAll();
            if (songs.isEmpty()) {
                throw new ServerException(ServerErrorCode.DATABASE_WRONG_POOL_SONG);
            }

            response.setSongs(songs);
        } catch (ServerException e) {
            response.setError(e.getErrorCode().toString());
            response.setErrorMessage(e.getErrorCode().getErrorString());
        }
        return gson.toJson(response);
    }

    public String getSongsByComposers(String requestJsonString) {
        GetSongsByComposersDtoRequest request = new Gson().fromJson(requestJsonString, GetSongsByComposersDtoRequest.class);
        GetSongsByComposersDtoResponse response = new GetSongsByComposersDtoResponse();
        try {
            request.validate();

            List<Song> songs = songRepository.findSongsByComposers(request.getComposers());
            if (songs.isEmpty()) {
                throw new ServerException(ServerErrorCode.DATABASE_WRONG_POOL_SONG);
            }

            response.setSongs(songs);
        } catch (ServerException e) {
            response.setError(e.getErrorCode().toString());
            response.setErrorMessage(e.getErrorCode().getErrorString());
        }
        return gson.toJson(response);
    }

    public String getSongsByAuthorOfWords(String requestJsonString) {
        GetSongsByAuthorOfWordsDtoRequest request = new Gson().fromJson(requestJsonString, GetSongsByAuthorOfWordsDtoRequest.class);
        GetSongsByAuthorOfWordsDtoResponse response = new GetSongsByAuthorOfWordsDtoResponse();
        try {
            request.validate();

            List<Song> songs = songRepository.findSongsByAuthorOfWords(request.getAuthorOfWords());
            if (songs.isEmpty()) {
                throw new ServerException(ServerErrorCode.DATABASE_WRONG_POOL_SONG);
            }

            response.setSongs(songs);
        } catch (ServerException e) {
            response.setError(e.getErrorCode().toString());
            response.setErrorMessage(e.getErrorCode().getErrorString());
        }
        return gson.toJson(response);
    }

    public String getSongsBySongArtist(String requestJsonString) {
        GetSongsBySongArtistDtoRequest request = new Gson().fromJson(requestJsonString, GetSongsBySongArtistDtoRequest.class);
        GetSongsBySongArtistDtoResponse response = new GetSongsBySongArtistDtoResponse();
        try {
            request.validate();

            List<Song> songs = songRepository.findSongBySongArtist(request.getSongArtist());
            if (songs.isEmpty()) {
                throw new ServerException(ServerErrorCode.DATABASE_WRONG_POOL_SONG);
            }

            response.setSongs(songs);
        } catch (ServerException e) {
            response.setError(e.getErrorCode().toString());
            response.setErrorMessage(e.getErrorCode().getErrorString());
        }
        return gson.toJson(response);
    }

    public String getConcert(String requestJsonString) {
        GetConcertDtoRequest request = new Gson().fromJson(requestJsonString, GetConcertDtoRequest.class);
        GetConcertDtoResponse response = new GetConcertDtoResponse();
        try {
            request.validate();

            List<Song> allSongs = songRepository.findAll();

            List<Song> sortedSongs = allSongs.stream()
                    .sorted((o1, o2) ->
                            Integer.compare((
                                            o2.getRating().values().stream().reduce((acc, x) -> acc + x).orElse(0)),
                                    o1.getRating().values().stream().reduce((acc, x) -> acc + x).orElse(0)))
                    .collect(Collectors.toList());
            int totalTime = 0;
            List<Concert> concert = new ArrayList<>();
            for (Song song : sortedSongs) {
                if (totalTime + song.getSongTiming() <= 3600) {
                    concert.add(new Concert(song));
                    totalTime += song.getSongTiming() + 10;
                }
            }
            if (concert.isEmpty()) {
                throw new ServerException(ServerErrorCode.DATABASE_WRONG_POOL_SONG);
            }

            response.setConcert(concert);
        } catch (ServerException e) {
            response.setError(e.getErrorCode().toString());
            response.setErrorMessage(e.getErrorCode().getErrorString());
        }
        return gson.toJson(response);
    }

    private void checkIsTrueSongAndUser(Song song, User user) throws ServerException {
        if (song == null) {
            throw new ServerException(ServerErrorCode.DATABASE_SONG_NOT_FOUND);
        }
        if (user == null) {
            throw new ServerException(ServerErrorCode.DATABASE_NOT_FOUND);
        }
    }

    private void checkIsTrueSongAndUserAndComment(Song song, User user, Comment commentEdit) throws ServerException {
        checkIsTrueSongAndUser(song, user);
        if (commentEdit == null) {
            throw new ServerException(ServerErrorCode.DATABASE_COMMENT_NOT_FOUND);
        }
    }
}
