package ml.antanaks.school.concert.service;

import com.google.gson.Gson;
import ml.antanaks.school.concert.dto.song.comment.remove.RemoveCommentDtoRequest;
import ml.antanaks.school.concert.dto.song.remove.RemoveSongDtoRequest;
import ml.antanaks.school.concert.dto.user.leave.LeaveUserDtoRequest;
import ml.antanaks.school.concert.dto.user.leave.LeaveUserDtoResponse;
import ml.antanaks.school.concert.dto.user.login.LoginUserDtoRequest;
import ml.antanaks.school.concert.dto.user.login.LoginUserDtoResponse;
import ml.antanaks.school.concert.dto.user.logout.LogoutUserDtoRequest;
import ml.antanaks.school.concert.dto.user.logout.LogoutUserDtoResponse;
import ml.antanaks.school.concert.dto.user.register.RegisterUserDtoRequest;
import ml.antanaks.school.concert.dto.user.register.RegisterUserDtoResponse;
import ml.antanaks.school.concert.entity.Song;
import ml.antanaks.school.concert.entity.User;
import ml.antanaks.school.concert.exception.ServerErrorCode;
import ml.antanaks.school.concert.exception.ServerException;
import ml.antanaks.school.concert.repository.SongRepository;
import ml.antanaks.school.concert.repository.UserRepository;
import ml.antanaks.school.concert.security.jwt.JwtTokenProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private SongRepository songRepository;

    @Autowired
    private SongService songService;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtTokenProvider jwtTokenProvider;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    public String registerUser(String requestJsonString) {
        RegisterUserDtoRequest request = new Gson().fromJson(requestJsonString, RegisterUserDtoRequest.class);
        RegisterUserDtoResponse response = new RegisterUserDtoResponse();
        try {
            request.validate();
            User user = new User(request.getFirstName(),
                    request.getLastName(),
                    request.getLogin(),
                    bCryptPasswordEncoder.encode(request.getPassword()));
            if (userRepository.findUserByLogin(user.getLogin()) != null) {
                throw new ServerException(ServerErrorCode.DATABASE_EXIST_LOGIN);
            }
            user.setToken(jwtTokenProvider.generateToken(user.getLogin()));
            userRepository.save(user);
            response.setToken(user.getToken());
        } catch (ServerException e) {
            response.setError(e.getErrorCode().toString());
            response.setErrorMessage(e.getErrorCode().getErrorString());
        }
        return new Gson().toJson(response);
    }

    public String logoutUser(String requestJsonString) {
        LogoutUserDtoRequest request = new Gson().fromJson(requestJsonString, LogoutUserDtoRequest.class);
        LogoutUserDtoResponse response = new LogoutUserDtoResponse();
        try {
            request.validate();
            User user = userRepository.findUserByToken(request.getToken());
            if (user == null) {
                throw new ServerException(ServerErrorCode.DATABASE_NOT_FOUND);
            }
            user.setToken(null);
            userRepository.save(user);
            response.setMessage("Пользователь упешно вышел");
        } catch (ServerException e) {
            response.setError(e.getErrorCode().toString());
            response.setErrorMessage(e.getErrorCode().getErrorString());
        }
        return new Gson().toJson(response);
    }

    public String loginUser(String requestJsonString) {
        LoginUserDtoRequest request = new Gson().fromJson(requestJsonString, LoginUserDtoRequest.class);
        LoginUserDtoResponse responseDto = new LoginUserDtoResponse();
        try {
            request.validate();

            String login = request.getLogin();
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(login, request.getPassword()));
            User user = userRepository.findUserByLogin(login);
            if (user == null) {
                throw new ServerException(ServerErrorCode.DATABASE_NOT_FOUND_USER);
            }
            user.setToken(jwtTokenProvider.generateToken(login));
            userRepository.save(user);
            responseDto.setToken(user.getToken());
        } catch (ServerException e) {
            responseDto.setError(e.getErrorCode().toString());
            responseDto.setErrorMessage(e.getErrorCode().getErrorString());
        } catch (BadCredentialsException e) {
            responseDto.setError(e.getMessage());
        }
        return new Gson().toJson(responseDto);
    }

    public String leaveUser(String requestJsonString) {
        LeaveUserDtoRequest request = new Gson().fromJson(requestJsonString, LeaveUserDtoRequest.class);
        LeaveUserDtoResponse response = new LeaveUserDtoResponse();
        try {
            request.validate();

            User user = userRepository.findUserByToken(request.getToken());
            if (user == null) {
                throw new ServerException(ServerErrorCode.DATABASE_NOT_FOUND);
            }

            List<Song> allSongs = songRepository.findAll();
            if (allSongs.isEmpty()) {
                throw new ServerException(ServerErrorCode.DATABASE_WRONG_POOL_SONG);
            }

            List<Song> collect = allSongs.stream().filter(song -> song.getUser().getLogin().equals(user.getLogin())).collect(Collectors.toList());
            for (Song song : collect) {
                songService.removeSong(new RemoveSongDtoRequest(song.getSongTitle(), request.getToken()));
            }

            for (Song song : allSongs) {
                if (song.getComments().stream().anyMatch(c -> c.getCreatorLogin().equals(user.getLogin()))) {
                    songService.removeComment(new RemoveCommentDtoRequest(song.getSongTitle(), request.getToken()));
                }
            }

            user.getSongs().clear();
            userRepository.delete(user);

            response.setMessage("Пользователь упешно покинул сервер");
        } catch (ServerException e) {
            response.setError(e.getErrorCode().toString());
            response.setErrorMessage(e.getErrorCode().getErrorString());
        }
        return new Gson().toJson(response);
    }
}