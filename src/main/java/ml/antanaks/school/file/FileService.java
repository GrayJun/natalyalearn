package ml.antanaks.school.file;

import com.google.gson.Gson;
import ml.antanaks.school.ttschool.Trainee;
import ml.antanaks.school.ttschool.TrainingException;
import ml.antanaks.school.windows.v4.Point;
import ml.antanaks.school.windows.v4.RectButton;
import ml.antanaks.school.windows.v4.base.WindowException;
import ml.antanaks.school.windows.v4.base.WindowState;

import java.io.*;
import java.util.Scanner;

public class FileService {

    private FileService() {
    }

    public static void writeByteArrayToBinaryFile(String fileName, byte[] array) throws IOException {
        try (FileOutputStream out = new FileOutputStream(fileName)) {
            out.write(array);
        } catch (IOException e) {
            throw new FileNotFoundException();
        }
    }

    public static void writeByteArrayToBinaryFile(File file, byte[] array) throws IOException {
        writeByteArrayToBinaryFile(file.getPath(), array);
    }

    public static byte[] readByteArrayFromBinaryFile(String fileName) throws IOException {
        try (InputStream inputStream = new FileInputStream(fileName)) {
            byte[] newArray = new byte[inputStream.available()];
            int j = 0;
            int temp;
            while ((temp = inputStream.read()) != -1) {
                newArray[j++] = (byte) temp;
            }
            return newArray;
        } catch (IOException e) {
            throw new FileNotFoundException();
        }
    }

    public static byte[] readByteArrayFromBinaryFile(File file) throws IOException {
        return readByteArrayFromBinaryFile(file.getPath());
    }

    public static byte[] writeAndReadByteArrayUsingByteStream(byte[] array) throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        byteArrayOutputStream.write(array);

        try (ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(byteArrayOutputStream.toByteArray())) {
            int size = array.length;
            byte[] newArray = new byte[size % 2 == 0 ? size / 2 : size / 2 + 1];
            int i = 0;
            int j = 0;
            int temp;
            while ((temp = byteArrayInputStream.read()) != -1) {
                if (i++ % 2 == 0) {
                    newArray[j++] = (byte) temp;
                }
            }
            return newArray;
        } catch (IOException e) {
            throw new FileNotFoundException();
        }
    }

    public static void writeByteArrayToBinaryFileBuffered(String fileName, byte[] array) throws IOException {
        try (BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(new FileOutputStream(fileName))) {
            bufferedOutputStream.write(array);
        } catch (IOException e) {
            throw new FileNotFoundException();
        }
    }

    public static void writeByteArrayToBinaryFileBuffered(File file, byte[] array) throws IOException {
        writeByteArrayToBinaryFileBuffered(file.getPath(), array);
    }

    public static byte[] readByteArrayFromBinaryFileBuffered(String fileName) throws IOException {
        try (BufferedInputStream bis = new BufferedInputStream(new FileInputStream(fileName))) {
            byte[] newArray = new byte[bis.available()];
            int j = 0;
            int temp;
            while ((temp = bis.read()) != -1) {
                newArray[j++] = (byte) temp;
            }
            return newArray;
        } catch (IOException e) {
            throw new FileNotFoundException();
        }
    }

    public static byte[] readByteArrayFromBinaryFileBuffered(File file) throws IOException {
        return readByteArrayFromBinaryFileBuffered(file.getPath());
    }

    public static void writeRectButtonToBinaryFile(File file, RectButton rectButton) throws IOException {
        try (DataOutputStream dos = new DataOutputStream(new FileOutputStream(file))) {
            dos.writeInt(rectButton.getTopLeft().getX());
            dos.writeInt(rectButton.getTopLeft().getY());
            dos.writeInt(rectButton.getBottomRight().getX());
            dos.writeInt(rectButton.getBottomRight().getY());
            dos.writeUTF(rectButton.getState().toString());
            dos.writeUTF(rectButton.getText());
        } catch (IOException e) {
            throw new FileNotFoundException();
        }
    }

    public static RectButton readRectButtonFromBinaryFile(File file) throws IOException, WindowException {
        try (DataInputStream dis = new DataInputStream(new FileInputStream(file))) {
            RectButton rectButton = new RectButton(new Point(dis.readInt(), dis.readInt()),
                    new Point(dis.readInt(), dis.readInt()), dis.readUTF(), dis.readUTF());
            return rectButton;
        } catch (IOException e) {
            throw new FileNotFoundException();
        }
    }

    public static void writeRectButtonArrayToBinaryFile(File file, RectButton[] rects) throws IOException {
        try (DataOutputStream dos = new DataOutputStream(new FileOutputStream(file))) {
            for (RectButton rect : rects) {
                dos.writeInt(rect.getTopLeft().getX());
                dos.writeInt(rect.getTopLeft().getY());
                dos.writeInt(rect.getBottomRight().getX());
                dos.writeInt(rect.getBottomRight().getY());
            }
        } catch (IOException e) {
            throw new FileNotFoundException();
        }
    }

    public static void modifyRectButtonArrayInBinaryFile(File file) throws IOException {
        try (DataInputStream dis = new DataInputStream(new FileInputStream(file))) {
            int size = dis.available() / 4;
            int[] intArray = new int[size];
            for (int i = 0; i < size; i++) {
                intArray[i++] = dis.readInt() + 1;
                intArray[i] = dis.readInt();
            }
            try (DataOutputStream dos = new DataOutputStream(new FileOutputStream(file))) {
                for (int value : intArray) {
                    dos.writeInt(value);
                }
            }
        } catch (IOException e) {
            throw new FileNotFoundException();
        }
    }

    public static RectButton[] readRectButtonArrayFromBinaryFile(File file) throws IOException, WindowException {
        try (DataInputStream dis = new DataInputStream(new FileInputStream(file))) {
            int size = dis.available() / 4;
            int[] intArray = new int[size];
            for (int i = 0; i < size; i++) {
                intArray[i] = dis.readInt();
            }
            RectButton[] rectButtons = new RectButton[size / 4];
            int j = 0;
            for (int i = 0; i < intArray.length / 4; i++) {
                rectButtons[i] = new RectButton(new Point(intArray[j++], intArray[j++]),
                        new Point(intArray[j++], intArray[j++]), "ACTIVE", "OK");
            }
            return rectButtons;
        } catch (IOException e) {
            throw new FileNotFoundException();
        }
    }

    public static void writeRectButtonToTextFileOneLine(File file, RectButton rectButton) throws IOException {
        try (FileWriter fileWriter = new FileWriter(file)) {
            String string = rectButton.getTopLeft().getX() +
                    " " + rectButton.getTopLeft().getY() +
                    " " + rectButton.getBottomRight().getX() +
                    " " + rectButton.getBottomRight().getY() +
                    " " + rectButton.getState() +
                    " " + rectButton.getText();
            fileWriter.write(string);
        } catch (IOException e) {
            throw new FileNotFoundException();
        }
    }

    public static RectButton readRectButtonFromTextFileOneLine(File file) throws IOException, WindowException {
        try (Scanner scanner = new Scanner(file)) {
            String text = "";
            while (scanner.hasNextLine()) {
                text = scanner.nextLine();
            }
            String[] s = text.split(" ");
            RectButton rectButton;
            rectButton = new RectButton(new Point(Integer.parseInt(s[0]), Integer.parseInt(s[1])),
                    new Point(Integer.parseInt(s[2]), Integer.parseInt(s[3])), WindowState.fromString(s[4]), s[5]);
            return rectButton;
        } catch (IOException e) {
            throw new FileNotFoundException();
        }
    }

    public static void writeRectButtonToTextFileSixLines(File file, RectButton rectButton) throws IOException {
        try (FileWriter fileWriter = new FileWriter(file)) {
            String string = rectButton.getTopLeft().getX() +
                    "\n" + rectButton.getTopLeft().getY() +
                    "\n" + rectButton.getBottomRight().getX() +
                    "\n" + rectButton.getBottomRight().getY() +
                    "\n" + rectButton.getState() +
                    "\n" + rectButton.getText();
            fileWriter.write(string);
        } catch (IOException e) {
            throw new FileNotFoundException();
        }
    }

    public static RectButton readRectButtonFromTextFileSixLines(File file) throws IOException, WindowException {
        try (Scanner scanner = new Scanner(file)) {
            String[] s = new String[6];
            while (scanner.hasNextLine()) {
                for (int i = 0; i < s.length; i++) {
                    s[i] = scanner.nextLine();
                }
            }
            RectButton rectButton;
            rectButton = new RectButton(new Point(Integer.parseInt(s[0]), Integer.parseInt(s[1])),
                    new Point(Integer.parseInt(s[2]), Integer.parseInt(s[3])), WindowState.fromString(s[4]), s[5]);
            return rectButton;
        } catch (IOException e) {
            throw new FileNotFoundException();
        }
    }

    public static void writeTraineeToTextFileOneLine(File file, Trainee trainee) throws IOException {
        try (BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(
                new FileOutputStream(file), "UTF-8"))) {
            String string = trainee.getFirstName() +
                    " " + trainee.getLastName() +
                    " " + trainee.getRating();
            bw.write(string);
        } catch (IOException e) {
            throw new FileNotFoundException();
        }
    }

    public static Trainee readTraineeFromTextFileOneLine(File file) throws IOException, TrainingException {
        try (Scanner scanner = new Scanner(file)) {
            Trainee trainee;
            String text = "";
            while (scanner.hasNextLine()) {
                text = scanner.nextLine();
            }
            String[] s = text.split(" ");
            trainee = new Trainee(s[0], s[1], Integer.parseInt(s[2]));
            return trainee;
        } catch (IOException e) {
            throw new FileNotFoundException();
        }
    }

    public static void writeTraineeToTextFileThreeLines(File file, Trainee trainee) throws IOException {
        try (BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(
                new FileOutputStream(file), "UTF-8"))) {
            String string = trainee.getFirstName() +
                    "\n" + trainee.getLastName() +
                    "\n" + trainee.getRating();
            bw.write(string);
        } catch (IOException e) {
            throw new FileNotFoundException();
        }
    }

    public static Trainee readTraineeFromTextFileThreeLines(File file) throws IOException, TrainingException {
        try (Scanner scanner = new Scanner(file)) {
            Trainee trainee;
            String[] s = new String[3];
            while (scanner.hasNextLine()) {
                for (int i = 0; i < s.length; i++) {
                    s[i] = scanner.nextLine();
                }
            }
            trainee = new Trainee(s[0], s[1], Integer.parseInt(s[2]));
            return trainee;
        } catch (IOException e) {
            throw new FileNotFoundException();
        }
    }

    public static void serializeTraineeToBinaryFile(File file, Trainee trainee) throws IOException {
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(file))) {
            oos.writeObject(trainee);
        } catch (IOException e) {
            throw new FileNotFoundException();
        }
    }

    public static Trainee deserializeTraineeFromBinaryFile(File file) throws IOException, ClassNotFoundException { // можно уменьшить объем кода
        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file))) {
            return (Trainee) ois.readObject();
        } catch (IOException e) {
            throw new FileNotFoundException();
        }
    }

    public static String serializeTraineeToJsonString(Trainee trainee) {
        return new Gson().toJson(trainee);
    }

    public static Trainee deserializeTraineeFromJsonString(String json) {
        return new Gson().fromJson(json, Trainee.class);
    }

    public static void serializeTraineeToJsonFile(File file, Trainee trainee) throws IOException {
        try (FileWriter fw = new FileWriter(file)) {
            fw.write(serializeTraineeToJsonString(trainee));
        } catch (IOException e) {
            throw new FileNotFoundException();
        }
    }

    public static Trainee deserializeTraineeFromJsonFile(File file) throws IOException {
        try (Scanner sc = new Scanner(file)) {
            String string = null;
            while (sc.hasNextLine()) {
                string = sc.nextLine();
            }
            return deserializeTraineeFromJsonString(string);
        } catch (IOException e) {
            throw new FileNotFoundException();
        }
    }
}