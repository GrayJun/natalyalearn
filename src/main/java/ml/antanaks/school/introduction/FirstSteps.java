package ml.antanaks.school.introduction;

import java.util.Arrays;

public class FirstSteps {

    //1
    public int sum(int x, int y) {
        return x + y;
    }

    //2
    public int mul(int x, int y) {
        return x * y;
    }

    //3
    public int div(int x, int y) {
        return x / y;
    }

    //4
    public int mod(int x, int y) {
        return x % y;
    }

    //5
    public boolean isEqual(int x, int y) {
        return x == y;
    }

    //6
    public boolean isGreater(int x, int y) {
        return x > y;
    }

    //7
    public boolean isInsideRect(int xLeft, int yTop, int xRight, int yBottom, int x, int y) {
        return (x <= xRight && x >= xLeft) && (y <= yBottom && y >= yTop);
    }

    //8
    public int sum(int[] array) {
        int summ = 0;
        for (int j : array) {
            summ += j;
        }
        return summ;
    }

    //9
    public int mul(int[] array) {
        int mull = 1;
        if (array.length == 0) return 0;
        for (int j : array) {
            mull *= j;
        }
        return mull;
    }

    //10
    public int min(int[] array) {
        int minValue = Integer.MAX_VALUE;
        for (int j : array) {
            minValue = Math.min(minValue, j);
        }
        return minValue;
    }

    //11
    public int max(int[] array) {
        int max = Integer.MIN_VALUE;
        for (int i : array) {
            if (i > max) {
                max = i;
            }
        }
        return max;
    }

    //12
    public double average(int[] array) {
        if (array.length == 0) return 0;
        int sumValue = 0;
        for (int j : array) {
            sumValue += j;
        }
        return (double) sumValue / array.length;
    }

    //13
    public boolean isSortedDescendant(int[] array) {
        if (!(array.length == 0)) {
            for (int i = 0; i < array.length - 1; i++) {
                if (!(array[i] > array[i + 1])) {
                    return false;
                }
            }
        }
        return true;
    }

    //14
    public void cube(int[] array) {
        for (int i = 0; i < array.length; i++) {
            array[i] = array[i] * array[i] * array[i];
        }
    }

    //15
    public boolean find(int[] array, int value) {
        for (int i = 0; i < array.length; i++) {
            if (array[i] == value) {
                return true;
            }
        }
        return false;
    }

    //16
    public void reverse(int[] array) {
        int size = array.length;
        int maxIndex = size - 1;
        for (int i = 0; i < size / 2; i++) {
            int temp = array[i];
            array[i] = array[maxIndex - i];
            array[maxIndex - i] = temp;
        }
    }

    //17 TODO cleanup code -DONE
    public boolean isPalindrome(int[] array) {
        int size = array.length;
        int maxIndex = size - 1;
        for (int i = 0; i < size / 2; i++) {
            if (!(array[i] == array[maxIndex - i])) {
                return false;
            }
        }
        return true;
    }

    //18 TODO call sum
    public int sum(int[][] matrix) {
        int summ = 0;
        for (int[] array : matrix) {
            summ += sum(array);
        }

//        long summ = 0;
//        for (int i = 0; i < matrix.length; i++) {
//            for (int j = 0; j < matrix[i].length; j++) {
//                summ += matrix[i][j];
//            }
//        }
        return summ;
    }

    //19 todo call max - DONE
    public int max(int[][] matrix) {
        int max = Integer.MIN_VALUE;
        for (int[] array : matrix) {
            if (max(array) > max) {
                max = max(array);
            }
        }

        return max;

//        int max = Integer.MIN_VALUE;
//        for (int i = 0; i < matrix.length; i++) {
//            for (int j = 0; j < matrix[i].length; j++) {
//                if (matrix[i][j] > max) {
//                    max = matrix[i][j];
//                }
//            }
//        }
//        return max;
    }

    //20 todo cleanup - DONE
    public int diagonalMax(int[][] matrix) {
        int max = Integer.MIN_VALUE;
        for (int i = 0; i < matrix.length; i++) {
            if (matrix[i][i] > max) {
                max = matrix[i][i];
            }
        }
        return max;
    }

    //21
    public boolean isSortedDescendant(int[][] matrix) {
        for (int[] array : matrix) {
            if (!isSortedDescendant(array)) {
                return false;
            }
        }
        return true;
    }

}
