package ml.antanaks.school.matrix;

import java.util.*;

public class MatrixNonSimilarRows {

    private final int[][] matrix;

    public MatrixNonSimilarRows(int[][] matrix) {
        this.matrix = matrix;
    }

    public List<int[]> getNonSimilarRows() {
        Map<Set<Integer>, int[]> map = new LinkedHashMap<>();
        for (int[] ints : matrix) {
            Set<Integer> setInteger = new TreeSet<>();
            for (int i : ints) {
                setInteger.add(i);
            }
            map.put(setInteger, ints);
        }
        List<int[]> listNonSimilarRows = new ArrayList<>();
        for (Map.Entry<Set<Integer>, int[]> entry : map.entrySet()) {
            listNonSimilarRows.add(entry.getValue());
        }
        return listNonSimilarRows;
    }
}