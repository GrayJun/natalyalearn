package ml.antanaks.school.ttschool;

import java.util.*;

public class Group {

    private String name;
    private String room;
    private List<Trainee> trainees;

    public Group(String name, String room) throws TrainingException {
        setName(name);
        setRoom(room);
        trainees = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) throws TrainingException {
        if (name == null || name.length() == 0) {
            throw new TrainingException(TrainingErrorCode.GROUP_WRONG_NAME);
        } else {
            this.name = name;
        }
    }

    public String getRoom() {
        return room;
    }

    public void setRoom(String room) throws TrainingException {
        if (room == null || room.length() == 0) {
            throw new TrainingException(TrainingErrorCode.GROUP_WRONG_ROOM);
        } else {
            this.room = room;
        }
    }

    public List<Trainee> getTrainees() {
        return trainees;
    }

    public void addTrainee(Trainee trainee) {
        trainees.add(trainee);
    }

    public void removeTrainee(Trainee trainee) throws TrainingException {
        if (!trainees.contains(trainee)) {
            throw new TrainingException(TrainingErrorCode.TRAINEE_NOT_FOUND);
        } else {
            trainees.remove(trainee);
        }
    }

    public void removeTrainee(int index) throws TrainingException {
        if (index >= trainees.size()) {
            throw new TrainingException(TrainingErrorCode.TRAINEE_NOT_FOUND);
        } else {
            removeTrainee(trainees.get(index));
        }
    }

    public Trainee getTraineeByFirstName(String firstName) throws TrainingException {
        Trainee value = null;
        for (Trainee trainee : trainees) {
            if (trainee.getFirstName().equals(firstName)) {
                value = trainee;
            }
        }
        if (value == null) {
            throw new TrainingException(TrainingErrorCode.TRAINEE_NOT_FOUND);
        }
        return value;
    }

    public Trainee getTraineeByFullName(String fullName) throws TrainingException {
        Trainee value = null;
        for (Trainee trainee : trainees) {
            if (trainee.getFullName().equals(fullName)) {
                value = trainee;
            }
        }
        if (value == null) {
            throw new TrainingException(TrainingErrorCode.TRAINEE_NOT_FOUND);
        }
        return value;
    }

    public void sortTraineeListByFirstNameAscendant() {
        trainees.sort(Comparator.comparing(Trainee::getFirstName));
    }

    public void sortTraineeListByRatingDescendant() {
        trainees.sort((o1, o2) -> Integer.compare(o2.getRating(), o1.getRating()));
    }

    public void reverseTraineeList() {
        Collections.reverse(trainees);
    }

    public void rotateTraineeList(int positions) {
        Collections.rotate(trainees, positions);
    }

    public List<Trainee> getTraineesWithMaxRating() throws TrainingException {
        if (trainees.size() == 0) {
            throw new TrainingException(TrainingErrorCode.TRAINEE_NOT_FOUND);
        }
        Trainee maxTrainee = Collections.max(trainees, Comparator.comparingInt(Trainee::getRating));
        List<Trainee> traineeList = new ArrayList<>();
        for (Trainee trainee : trainees) {
            if (trainee.getRating() == maxTrainee.getRating()) {
                traineeList.add(trainee);
            }
        }
        return traineeList;
    }

    public boolean hasDuplicates() {
        Set<Trainee> traineeSet = new HashSet<>(trainees);
        return traineeSet.size() != trainees.size();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Group group = (Group) o;
        return name.equals(group.name) &&
                trainees.equals(group.trainees);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, trainees);
    }
}