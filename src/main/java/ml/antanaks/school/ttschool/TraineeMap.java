package ml.antanaks.school.ttschool;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class TraineeMap {

    private Map<Trainee, String> traineeStringMap;

    public TraineeMap() {
        traineeStringMap = new HashMap<>();
    }

    public void addTraineeInfo(Trainee trainee, String institute) throws TrainingException {
        if (traineeStringMap.containsKey(trainee)) {
            throw new TrainingException(TrainingErrorCode.DUPLICATE_TRAINEE);
        }
        traineeStringMap.put(trainee, institute);
    }

    public void replaceTraineeInfo(Trainee trainee, String institute) throws TrainingException {
        if (!traineeStringMap.containsKey(trainee)) {
            throw new TrainingException(TrainingErrorCode.TRAINEE_NOT_FOUND);
        }
        traineeStringMap.put(trainee, institute);
    }

    public void removeTraineeInfo(Trainee trainee) throws TrainingException {
        if (!traineeStringMap.containsKey(trainee)) {
            throw new TrainingException(TrainingErrorCode.TRAINEE_NOT_FOUND);
        }
        traineeStringMap.remove(trainee);
    }

    public int getTraineesCount() {
        return traineeStringMap.size();
    }

    public String getInstituteByTrainee(Trainee trainee) throws TrainingException {
        String value = null;
        for (Map.Entry<Trainee, String> trainee1 : traineeStringMap.entrySet()) {
            if (trainee1.getKey().equals(trainee)) {
                value = trainee1.getValue();
            }
        }
        if (value == null) {
            throw new TrainingException(TrainingErrorCode.TRAINEE_NOT_FOUND);
        }
        return value;
    }

    public Set<Trainee> getAllTrainees() {
        return traineeStringMap.keySet();
    }

    public Set<String> getAllInstitutes() {
        return new HashSet<>(traineeStringMap.values());
    }

    public boolean isAnyFromInstitute(String institute) {
        return traineeStringMap.containsValue(institute);
    }
}