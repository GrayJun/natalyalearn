package ml.antanaks.school.ttschool;

import java.util.ArrayDeque;
import java.util.Queue;

public class TraineeQueue {

    private Queue<Trainee> traineeQueue;

    public TraineeQueue() {
        traineeQueue = new ArrayDeque<>();
    }

    public void addTrainee(Trainee trainee) {
        traineeQueue.offer(trainee);
    }

    public Trainee removeTrainee() throws TrainingException {
        Trainee poll = traineeQueue.poll();
        if (poll == null) {
            throw new TrainingException(TrainingErrorCode.EMPTY_TRAINEE_QUEUE);
        }
        return poll;
    }

    public boolean isEmpty() {
        Trainee peek = traineeQueue.peek();
        return peek == null;
    }
}