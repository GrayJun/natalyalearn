package ml.antanaks.school.windows.managers;

import ml.antanaks.school.windows.v4.Desktop;
import ml.antanaks.school.windows.v4.base.Window;
import ml.antanaks.school.windows.v4.base.WindowErrorCode;
import ml.antanaks.school.windows.v4.base.WindowException;
import ml.antanaks.school.windows.v4.cursors.Cursor;

public class ArrayManager<T extends Window> {

    private T[] windows;

    public ArrayManager(T[] windows) throws WindowException {
        for (T value : windows) {
            if (value == null) {
                throw new WindowException(WindowErrorCode.NULL_WINDOW);
            } else this.windows = windows;
        }
    }

    public T[] getWindows() {
        return windows;
    }

    public void setWindows(T[] window) {
        this.windows = window;
    }

    public T getWindow(int i) {
        return windows[i];
    }

    public void setWindow(T window, int i) {
        for (int j = 0; j < this.windows.length; j++) {
            if (i == j) {
                this.windows[i] = window;
                break;
            }
        }
    }

    public boolean isSameSize(ArrayManager<? extends Window> arrayManager) {
        return this.windows.length == arrayManager.windows.length;
    }

    public boolean allWindowsFullyVisibleOnDesktop(Desktop desktop) {
        for (T window : windows) {
            if (!(window.isFullyVisibleOnDesktop(desktop))) {
                return false;
            }
        }
        return true;
    }

    public boolean anyWindowFullyVisibleOnDesktop(Desktop desktop) {
        for (T window : windows) {
            if (window.isFullyVisibleOnDesktop(desktop)) {
                return true;
            }
        }
        return false;
    }

    public Window getFirstWindowFromCursor(Cursor cursor) {
        for (T window : this.windows) {
            if (window.isInside(cursor.getX(), cursor.getY())) {
                return window;
            }
        }
        return null;
    }
}

