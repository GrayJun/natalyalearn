package ml.antanaks.school.windows.managers;

import ml.antanaks.school.windows.v4.Desktop;
import ml.antanaks.school.windows.v4.base.Window;
import ml.antanaks.school.windows.v4.base.WindowErrorCode;
import ml.antanaks.school.windows.v4.base.WindowException;

public class PairManager<T1 extends Window, T2 extends Window> {

    private T1 firstWindow;
    private T2 secondWindow;

    public PairManager(T1 firstWindow, T2 secondWindow) throws WindowException {
        if (firstWindow == null || secondWindow == null) {
            throw new WindowException(WindowErrorCode.NULL_WINDOW);
        } else {
            this.firstWindow = firstWindow;
            this.secondWindow = secondWindow;
        }
    }

    public static boolean allWindowsFullyVisibleOnDesktop(PairManager<? extends Window, ? extends Window> pairManager1, PairManager<? extends Window, ? extends Window> pairManager2, Desktop desktop) {
        return pairManager1.allWindowsFullyVisibleOnDesktop(pairManager1, desktop)
                && pairManager2.allWindowsFullyVisibleOnDesktop(pairManager2, desktop);
    }

    public T1 getFirstWindow() {
        return firstWindow;
    }

    public void setFirstWindow(T1 firstWindow) {
        this.firstWindow = firstWindow;
    }

    public T2 getSecondWindow() {
        return secondWindow;
    }

    public void setSecondWindow(T2 secondWindow) {
        this.secondWindow = secondWindow;
    }

    public boolean allWindowsFullyVisibleOnDesktop(PairManager<? extends Window, ? extends Window> pairManager, Desktop desktop) {
        return (pairManager.firstWindow).isFullyVisibleOnDesktop(desktop)
                && (pairManager.secondWindow).isFullyVisibleOnDesktop(desktop);
    }
}
