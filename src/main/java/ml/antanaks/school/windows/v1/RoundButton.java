package ml.antanaks.school.windows.v1;

import java.util.Objects;

public class RoundButton {

    private Point center;
    private int radius;
    private boolean active;

//    private int xCenter;
//    private int yCenter;

    //    1
    public RoundButton(Point center, int radius, boolean active) {
        this.center = center;
        this.radius = radius;
        this.active = active;
    }

    //    2
    public RoundButton(int xCenter, int yCenter, int radius, boolean active) {
        this(new Point(xCenter, yCenter), radius, active);
//        this.xCenter = xCenter;
//        this.yCenter = yCenter;
//        this.radius = radius;
//        this.active = active;
    }

    //    3
    public RoundButton(Point center, int radius) {
        this(center, radius, true);
//        this.center = center;
//        this.radius = radius;
    }

    //    4
    public RoundButton(int xCenter, int yCenter, int radius) {
        this(xCenter, yCenter, radius, true);
//        this.xCenter = xCenter;
//        this.yCenter = yCenter;
//        this.radius = radius;
    }

    //    5
    public Point getCenter() {
        return center;
    }

    //    6
    public int getRadius() {
        return radius;
    }

    // 7
    public boolean isActive() {
        return active;
    }

    // 8
    public void moveTo(int x, int y) {
        center.setX(x);
        center.setY(y);
    }

    //    9
    public void moveTo(Point point) {
        center = point;
    }

    //    10
    public void setCenter(int x, int y) {
        this.center.setX(x);
        this.center.setY(y);

    }

    // 11
    public void setRadius(int radius) {
        this.radius = radius;
    }

    // 12
    public void setActive(boolean active) {
        this.active = active;
    }

    //    13
    public void moveRel(int dx, int dy) {
        this.center.setX(center.getX() + dx);
        this.center.setY(center.getY() + dy);
    }

    //    14
    public void resize(double ratio) {
        int newRadius = (int) (getRadius() * ratio);
        radius = Math.max(newRadius, 1);
    }

    //    15
    public boolean isInside(int x, int y) {
        double distance = Math.pow((center.getX() - x), 2) + Math.pow((center.getY() - y), 2);
        return ((int) Math.sqrt(distance)) <= radius;
    }

    //    16
    public boolean isInside(Point point) {
        double distance = Math.pow((center.getX() - point.getX()), 2) + Math.pow((center.getY() - point.getY()), 2);
        return ((int) Math.sqrt(distance)) <= radius;
    }

    //    17
    public boolean isFullyVisibleOnDesktop(Desktop desktop) {
        return ((center.getX() - radius) >= 0
                && (center.getX() + radius) < desktop.getWidth())
                && ((center.getY() - radius) >= 0
                && (center.getY() + radius) < desktop.getHeight());
    }

    //    18
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RoundButton that = (RoundButton) o;
        return radius == that.radius &&
                active == that.active &&
                Objects.equals(center, that.center);
    }

    @Override
    public int hashCode() {
        return Objects.hash(center, radius, active);
    }
}
