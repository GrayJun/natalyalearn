package ml.antanaks.school.windows.v1;

public class WindowFactory {

    private static int countRectButton;
    private static int countRoundButton;

    //    Методы должны вызывать друг-друга - НЕ ПОНЯЛА
    public static RectButton createRectButton(Point leftTop, Point rightBottom, boolean active) {
        RectButton rectButton = new RectButton(leftTop, rightBottom, active);
        countRectButton ++;
        return rectButton;
    }

    public static RoundButton createRoundButton(Point center, int radius, boolean active) {
        RoundButton roundButton = new RoundButton(center, radius, active);
        countRoundButton ++;
        return roundButton;
    }

    public static int getRectButtonCount() {
        return countRectButton;
    }

    public static int getRoundButtonCount() {
        return countRoundButton;
    }

    public static int getWindowCount() {
        return countRectButton + countRoundButton;
    }

    public static void reset() {
        countRectButton = 0;
        countRoundButton = 0;
    }
}