package ml.antanaks.school.windows.v2;

import java.util.Arrays;
import java.util.Objects;

import static ml.antanaks.school.base.StringOperations.isLess;
import static ml.antanaks.school.base.StringOperations.reverse;

public class ListBox {

    private String[] lines;
    private Point topLeft;
    private Point bottomRight;
    private boolean active;

    public ListBox(Point topLeft, Point bottomRight, boolean active, String[] lines) {
        this.topLeft = topLeft;
        this.bottomRight = bottomRight;
        this.active = active;
        if (!(lines == null)) {
            this.lines = new String[lines.length];
            System.arraycopy(lines, 0, this.lines, 0, lines.length);
        }
    }

    public ListBox(int xLeft, int yTop, int width, int height, boolean active, String[] lines) {
        this(new Point(xLeft, yTop),
                new Point(xLeft + width - 1, yTop + height - 1),
                active,
                lines);
    }

    public ListBox(Point topLeft, Point bottomRight, String[] lines) {
        this(topLeft, bottomRight, true, lines);
    }

    public ListBox(int xLeft, int yTop, int width, int height, String[] lines) {
        this(xLeft, yTop, width, height, true, lines);
    }

    public Point getTopLeft() {
        return topLeft;
    }

    public Point getBottomRight() {
        return bottomRight;
    }

    public boolean isActive() {
        return active;
    }

    public void setTopLeft(Point topLeft) {
        this.topLeft = topLeft;
    }

    public void setBottomRight(Point bottomRight) {
        this.bottomRight = bottomRight;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public int getWidth() {
        return bottomRight.getX() - topLeft.getX() + 1;
    }

    public int getHeight() {
        return bottomRight.getY() - topLeft.getY() + 1;
    }

    public String[] getLines() {
        return lines;
    }

    public void setLines(String[] lines) {
        if (lines == null) {
            this.lines = null;
        } else {
            this.lines = new String[lines.length];
            System.arraycopy(lines, 0, this.lines, 0, lines.length);
        }
    }

    public String[] getLinesSlice(int from, int to) {
        if (lines == null) return null;
        int endArray = Math.min(lines.length, to);
        String[] strings = new String[endArray - from];
        int startArr = 0;
        for (int i = 0; i < lines.length; i++) {
            if (i >= from && i < endArray) {
                strings[startArr++] = lines[i];
            }
        }
        return strings;
    }

    public String getLine(int index) {
        for (int i = 0; i < lines.length; i++) {
            if (i == index) {
                return lines[i];
            }
        }
        return null;
    }

    public void setLine(int index, String line) {
        for (int i = 0; i < lines.length; i++) {
            if (i == index) {
                lines[i] = line;
            }
        }
    }

    public Integer findLine(String line) {
        if (lines == null) return null;
        for (int i = 0; i < lines.length; i++) {
            if (lines[i].equals(line)) {
                return i;
            }
        }
        return null;
    }

    public void reverseLineOrder() {
        if (!(lines == null)) {
            int size = lines.length;
            int maxIndex = size - 1;
            for (int i = 0; i < size / 2; i++) {
                String temp = lines[i];
                lines[i] = lines[maxIndex - i];
                lines[maxIndex - i] = temp;
            }
        }
    }

    public void reverseLines() {
        if (!(lines == null)) {
            for (int i = 0; i < lines.length; i++) {
                String value = reverse(lines[i]);
                setLine(i, value);
            }
        }
    }

    public void duplicateLines() {
        if (!(lines == null)) {
            String[] strings = new String[2 * lines.length];
            int j = 0;
            for (String line : lines) {
                strings[j++] = line;
                strings[j++] = line;
            }
            setLines(strings);
        }
    }

    public void removeOddLines() {
        if (!(lines == null)) {
            int lengthArr = lines.length % 2 == 0 ? lines.length : lines.length + 1;
            String[] strings = new String[lengthArr / 2];
            int j = 0;
            for (int i = 0; i < lines.length; i++) {
                if (i % 2 == 0) {
                    strings[j++] = lines[i];
                }
            }
            setLines(strings);
        }
    }

    public boolean isSortedDescendant() {
        if (!(lines == null)) {
            for (int i = 0; i < lines.length - 1; i++) {
                if (isLess(lines[i], lines[i + 1])) {
                    return false;
                }
            }
        }
        return true;
    }

    public void moveTo(int x, int y) {
        int width = getWidth() - 1;
        int height = getHeight() - 1;
        topLeft.moveTo(x, y);
        bottomRight.moveTo(topLeft.getX() + width, topLeft.getY() + height);
    }

    public void moveTo(Point point) {
        this.moveTo(point.getX(), point.getY());
    }

    public void moveRel(int dx, int dy) {
        int width = getWidth() - 1;
        int height = getHeight() - 1;
        topLeft.moveTo(topLeft.getX() + dx, topLeft.getY() + dy);
        bottomRight.moveTo(topLeft.getX() + width, topLeft.getY() + height);
    }

    public void resize(double ratio) {
        int width = (int) (getWidth() * ratio);
        int height = (int) (getHeight() * ratio);
        if (width < 1 || height < 1) {
            bottomRight.setX(topLeft.getX());
            bottomRight.setY(topLeft.getY());
        } else {
            bottomRight.setX(topLeft.getX() + width - 1);
            bottomRight.setY(topLeft.getY() + height - 1);
        }
    }

    public boolean isInside(int x, int y) {
        return (x >= topLeft.getX()
                && y >= topLeft.getY())
                && (x <= bottomRight.getX()
                && y <= bottomRight.getY());
    }

    public boolean isInside(Point point) {
        return (point.getX() >= topLeft.getX()
                && point.getY() >= topLeft.getY())
                && (point.getX() <= bottomRight.getX()
                && point.getY() <= bottomRight.getY());
    }

    public boolean isIntersects(ListBox listBox) {
        return (this.isInside(listBox.getTopLeft())
                || this.isInside(listBox.getBottomRight()))
                || (listBox.isInside(this.getTopLeft())
                || listBox.isInside(this.getBottomRight()));
    }

    public boolean isInside(ListBox listBox) {
        return (this.isInside(listBox.getTopLeft())
                && this.isInside(listBox.getBottomRight()))
                || (listBox.isInside(this.getTopLeft())
                && listBox.isInside(this.getBottomRight()));
    }

    public boolean isFullyVisibleOnDesktop(Desktop desktop) {
        return (getTopLeft().getX() >= 0
                && getBottomRight().getX() <= desktop.getWidth())
                && (getTopLeft().getY() >= 0
                && getBottomRight().getY() <= desktop.getHeight());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ListBox listBox = (ListBox) o;
        return active == listBox.active &&
                Arrays.equals(lines, listBox.lines) &&
                Objects.equals(topLeft, listBox.topLeft) &&
                Objects.equals(bottomRight, listBox.bottomRight);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(topLeft, bottomRight, active);
        result = 31 * result + Arrays.hashCode(lines);
        return result;
    }
}


