package ml.antanaks.school.windows.v2;

import java.util.Objects;

public class RoundButton {

    private String text;
    private Point center;
    private int radius;
    private boolean active;

    public RoundButton(Point center, int radius, boolean active, String text) {
        this.center = center;
        this.radius = radius;
        this.active = active;
        this.text = text;
    }

    public RoundButton(int xCenter, int yCenter, int radius, boolean active, String text) {
        this(new Point(xCenter, yCenter), radius, active, text);
    }

    public RoundButton(Point center, int radius, String text) {
        this(center, radius, true, text);
    }

    public RoundButton(int xCenter, int yCenter, int radius, String text) {
        this(xCenter, yCenter, radius, true, text);
    }

    public Point getCenter() {
        return center;
    }

    public int getRadius() {
        return radius;
    }

    public boolean isActive() {
        return active;
    }

    public void moveTo(int x, int y) {
        center.setX(x);
        center.setY(y);
    }

    public void moveTo(Point point) {
        center = point;
    }

    public void setCenter(int x, int y) {
        this.center.setX(x);
        this.center.setY(y);
    }

    public void setRadius(int radius) {
        this.radius = radius;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public void moveRel(int dx, int dy) {
        this.center.setX(center.getX() + dx);
        this.center.setY(center.getY() + dy);
    }

    public void resize(double ratio) {
        int newRadius = (int) (getRadius() * ratio);
        radius = Math.max(newRadius, 1);
    }

    public boolean isInside(int x, int y) {
        double distance = Math.pow((center.getX() - x), 2) + Math.pow((center.getY() - y), 2);
        return ((int) Math.sqrt(distance)) <= radius;
    }

    public boolean isInside(Point point) {
        double distance = Math.pow((center.getX() - point.getX()), 2) + Math.pow((center.getY() - point.getY()), 2);
        return ((int) Math.sqrt(distance)) <= radius;
    }

    public boolean isFullyVisibleOnDesktop(Desktop desktop) {
        return ((center.getX() - radius) >= 0
                && (center.getX() + radius) < desktop.getWidth())
                && ((center.getY() - radius) >= 0
                && (center.getY() + radius) < desktop.getHeight());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RoundButton that = (RoundButton) o;
        return radius == that.radius &&
                active == that.active &&
                Objects.equals(text, that.text) &&
                Objects.equals(center, that.center);
    }

    @Override
    public int hashCode() {
        return Objects.hash(text, center, radius, active);
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
