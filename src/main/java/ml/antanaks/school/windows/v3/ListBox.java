package ml.antanaks.school.windows.v3;

import ml.antanaks.school.windows.v3.base.RectWindow;

import java.util.Arrays;

import static ml.antanaks.school.base.StringOperations.isLess;
import static ml.antanaks.school.base.StringOperations.reverse;

public class ListBox extends RectWindow {

    private String[] lines;

    public ListBox(Point topLeft, Point bottomRight, boolean active, String[] lines) {
        super(topLeft, bottomRight, active);
        if (!(lines == null)) {
            this.lines = new String[lines.length];
            System.arraycopy(lines, 0, this.lines, 0, lines.length);
        }
    }

    public ListBox(int xLeft, int yTop, int width, int height, boolean active, String[] lines) {
        this(new Point(xLeft, yTop),
                new Point(xLeft + width - 1, yTop + height - 1),
                active, lines);
    }

    public ListBox(Point topLeft, Point bottomRight, String[] lines) {
        this(topLeft, bottomRight, true, lines);
    }

    public ListBox(int xLeft, int yTop, int width, int height, String[] lines) {
        this(xLeft, yTop, width, height, true, lines);
    }

    public String[] getLines() {
        return lines;
    }

    public void setLines(String[] lines) {
        if (lines == null) {
            this.lines = null;
        } else {
            this.lines = new String[lines.length];
            System.arraycopy(lines, 0, this.lines, 0, lines.length);
        }
    }

    public String[] getLinesSlice(int from, int to) {
        if (lines == null) return null;
        int endArray = Math.min(lines.length, to);
        String[] strings = new String[endArray - from];
        int startArr = 0;
        for (int i = 0; i < lines.length; i++) {
            if (i >= from && i < endArray) {
                strings[startArr++] = lines[i];
            }
        }
        return strings;
    }

    public String getLine(int index) {
        for (int i = 0; i < lines.length; i++) {
            if (i == index) {
                return lines[i];
            }
        }
        return null;
    }

    public void setLine(int index, String line) {
        for (int i = 0; i < lines.length; i++) {
            if (i == index) {
                lines[i] = line;
            }
        }
    }

    public Integer findLine(String line) {
        if (lines == null) return null;
        for (int i = 0; i < lines.length; i++) {
            if (lines[i].equals(line)) {
                return i;
            }
        }
        return null;
    }

    public void reverseLineOrder() {
        if (!(lines == null)) {
            int size = lines.length;
            int maxIndex = size - 1;
            for (int i = 0; i < size / 2; i++) {
                String temp = lines[i];
                lines[i] = lines[maxIndex - i];
                lines[maxIndex - i] = temp;
            }
        }
    }

    public void reverseLines() {
        if (!(lines == null)) {
            for (int i = 0; i < lines.length; i++) {
                String value = reverse(lines[i]);
                setLine(i, value);
            }
        }
    }

    public void duplicateLines() {
        if (!(lines == null)) {
            String[] strings = new String[2 * lines.length];
            int j = 0;
            for (String line : lines) {
                strings[j++] = line;
                strings[j++] = line;
            }
            setLines(strings);
        }
    }

    public void removeOddLines() {
        if (!(lines == null)) {
            int lengthArr = lines.length % 2 == 0 ? lines.length : lines.length + 1;
            String[] strings = new String[lengthArr / 2];
            int j = 0;
            for (int i = 0; i < lines.length; i++) {
                if (i % 2 == 0) {
                    strings[j++] = lines[i];
                }
            }
            setLines(strings);
        }
    }

    public boolean isSortedDescendant() {
        if (!(lines == null)) {
            for (int i = 0; i < lines.length - 1; i++) {
                if (isLess(lines[i], lines[i + 1])) {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        ListBox listBox = (ListBox) o;
        return Arrays.equals(lines, listBox.lines);
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + Arrays.hashCode(lines);
        return result;
    }
}

