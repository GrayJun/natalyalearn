package ml.antanaks.school.windows.v3;

public class WindowFactory {

    private static int countRectButton;
    private static int countRoundButton;

    public static RectButton createRectButton(Point leftTop, Point rightBottom, boolean active, String text) {
        RectButton rectButton = new RectButton(leftTop, rightBottom, active, text);
        countRectButton++;
        return rectButton;
    }

    public static RoundButton createRoundButton(Point center, int radius, boolean active, String text) {
        RoundButton roundButton = new RoundButton(center, radius, active, text);
        countRoundButton++;
        return roundButton;
    }

    public static int getRectButtonCount() {
        return countRectButton;
    }

    public static int getRoundButtonCount() {
        return countRoundButton;
    }

    public static int getWindowCount() {
        return countRectButton + countRoundButton;
    }

    public static void reset() {
        countRectButton = 0;
        countRoundButton = 0;
    }
}
