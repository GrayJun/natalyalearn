package ml.antanaks.school.windows.v3.base;

import ml.antanaks.school.windows.v3.Desktop;
import ml.antanaks.school.windows.v3.Point;

import java.util.Objects;

public abstract class RectWindow extends Window {

    private Point topLeft;
    private Point bottomRight;

    public RectWindow(Point topLeft, Point bottomRight, boolean active) {
        super(active);
        this.topLeft = topLeft;
        this.bottomRight = bottomRight;
    }

    public Point getTopLeft() {
        return topLeft;
    }

    public Point getBottomRight() {
        return bottomRight;
    }

    public void setTopLeft(Point topLeft) {
        this.topLeft = topLeft;
    }

    public void setBottomRight(Point bottomRight) {
        this.bottomRight = bottomRight;
    }

    public int getWidth() {
        return bottomRight.getX() - topLeft.getX() + 1;
    }

    public int getHeight() {
        return bottomRight.getY() - topLeft.getY() + 1;
    }

    public boolean isIntersects(RectWindow rectWindow) {
        return (this.isInside(rectWindow.getTopLeft())
                || this.isInside(rectWindow.getBottomRight()))
                || (rectWindow.isInside(this.getTopLeft())
                || rectWindow.isInside(this.getBottomRight()));
    }

    public boolean isInside(RectWindow rectWindow) {
        return (this.isInside(rectWindow.getTopLeft())
                && this.isInside(rectWindow.getBottomRight()))
                || (rectWindow.isInside(this.getTopLeft())
                && rectWindow.isInside(this.getBottomRight()));
    }

    @Override
    public boolean isInside(int x, int y) {
        return (x >= topLeft.getX()
                && y >= topLeft.getY())
                && (x <= bottomRight.getX()
                && y <= bottomRight.getY());
    }

    @Override
    public boolean isInside(Point point) {
        return (point.getX() >= topLeft.getX()
                && point.getY() >= topLeft.getY())
                && (point.getX() <= bottomRight.getX()
                && point.getY() <= bottomRight.getY());
    }

    @Override
    public boolean isFullyVisibleOnDesktop(Desktop desktop) {
        return (getTopLeft().getX() >= 0
                && getBottomRight().getX() <= desktop.getWidth())
                && (getTopLeft().getY() >= 0
                && getBottomRight().getY() <= desktop.getHeight());
    }

    @Override
    public void moveTo(int x, int y) {
        int width = getWidth() - 1;
        int height = getHeight() - 1;
        topLeft.moveTo(x, y);
        bottomRight.moveTo(topLeft.getX() + width, topLeft.getY() + height);
    }

    @Override
    public void moveRel(int dx, int dy) {
        int width = getWidth() - 1;
        int height = getHeight() - 1;
        topLeft.moveTo(topLeft.getX() + dx, topLeft.getY() + dy);
        bottomRight.moveTo(topLeft.getX() + width, topLeft.getY() + height);
    }

    @Override
    public void resize(double ratio) {
        int width = (int) (getWidth() * ratio);
        int height = (int) (getHeight() * ratio);
        if (width < 1 || height < 1) {
            bottomRight.setX(topLeft.getX());
            bottomRight.setY(topLeft.getY());
        } else {
            bottomRight.setX(topLeft.getX() + width - 1);
            bottomRight.setY(topLeft.getY() + height - 1);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RectWindow that = (RectWindow) o;
        return topLeft.equals(that.topLeft) &&
                bottomRight.equals(that.bottomRight);
    }

    @Override
    public int hashCode() {
        return Objects.hash(topLeft, bottomRight);
    }
}
