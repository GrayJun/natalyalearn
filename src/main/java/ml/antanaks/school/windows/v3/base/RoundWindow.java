package ml.antanaks.school.windows.v3.base;

import ml.antanaks.school.windows.v3.Desktop;
import ml.antanaks.school.windows.v3.Point;

import java.util.Objects;

public abstract class RoundWindow extends Window {

    private Point center;
    private int radius;

    public RoundWindow(Point center, int radius, boolean active) {
        super(active);
        this.center = center;
        this.radius = radius;
    }

    public Point getCenter() {
        return center;
    }

    public int getRadius() {
        return radius;
    }

    public void setCenter(int x, int y) {
        this.center.setX(x);
        this.center.setY(y);
    }

    public void setRadius(int radius) {
        this.radius = radius;
    }

    @Override
    public boolean isInside(int x, int y) {
        double distance = Math.pow((center.getX() - x), 2) + Math.pow((center.getY() - y), 2);
        return ((int) Math.sqrt(distance)) <= radius;
    }

    @Override
    public boolean isInside(Point point) {
        double distance = Math.pow((center.getX() - point.getX()), 2) + Math.pow((center.getY() - point.getY()), 2);
        return ((int) Math.sqrt(distance)) <= radius;
    }

    @Override
    public boolean isFullyVisibleOnDesktop(Desktop desktop) {
        return ((center.getX() - radius) >= 0
                && (center.getX() + radius) < desktop.getWidth())
                && ((center.getY() - radius) >= 0
                && (center.getY() + radius) < desktop.getHeight());
    }

    @Override
    public void moveTo(int x, int y) {
        center.setX(x);
        center.setY(y);
    }

    @Override
    public void moveRel(int dx, int dy) {
        this.center.setX(center.getX() + dx);
        this.center.setY(center.getY() + dy);
    }

    @Override
    public void resize(double ratio) {
        int newRadius = (int) (getRadius() * ratio);
        radius = Math.max(newRadius, 1);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RoundWindow that = (RoundWindow) o;
        return radius == that.radius &&
                center.equals(that.center);
    }

    @Override
    public int hashCode() {
        return Objects.hash(center, radius);
    }
}
