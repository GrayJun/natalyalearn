package ml.antanaks.school.windows.v3.base;

import ml.antanaks.school.windows.v3.Desktop;
import ml.antanaks.school.windows.v3.Point;
import ml.antanaks.school.windows.v3.iface.Movable;
import ml.antanaks.school.windows.v3.iface.Resizable;

public abstract class Window implements Movable, Resizable {

    private boolean active;

    public Window(boolean active) {
        this.active = active;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public abstract boolean isInside(int x, int y);

    public abstract boolean isInside(Point point);

    public abstract boolean isFullyVisibleOnDesktop(Desktop desktop);
}
