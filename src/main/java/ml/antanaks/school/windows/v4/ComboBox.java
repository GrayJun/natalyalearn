package ml.antanaks.school.windows.v4;

import ml.antanaks.school.windows.v4.base.WindowErrorCode;
import ml.antanaks.school.windows.v4.base.WindowException;
import ml.antanaks.school.windows.v4.base.WindowState;

import java.util.Objects;

public class ComboBox extends ListBox {

    private Integer selected;

    public ComboBox(Point topLeft, Point bottomRight, WindowState state, String[] lines, Integer selected) throws WindowException {
        super(topLeft, bottomRight, state, lines);
        if (lines == null && selected != null) {
            throw new WindowException(WindowErrorCode.EMPTY_ARRAY);
        }
        if (selected != null) {
            if (!(selected > (lines.length - 1) || selected < 0)) {
                this.selected = selected;
            } else throw new WindowException(WindowErrorCode.WRONG_INDEX);
        }
    }

    public ComboBox(Point topLeft, Point bottomRight, String state, String[] lines, Integer selected) throws WindowException {
        this(topLeft, bottomRight, WindowState.fromString(state), lines, selected);
    }

    public ComboBox(int xLeft, int yTop, int width, int height, WindowState state, String[] lines, Integer selected) throws WindowException {
        this(new Point(xLeft, yTop),
                new Point(xLeft + width - 1, yTop + height - 1),
                state, lines, selected);
    }

    public ComboBox(int xLeft, int yTop, int width, int height, String state, String[] lines, Integer selected) throws WindowException {
        this(new Point(xLeft, yTop),
                new Point(xLeft + width - 1, yTop + height - 1),
                state, lines, selected);
    }

    public ComboBox(Point topLeft, Point bottomRight, String[] lines, Integer selected) throws WindowException {
        this(topLeft, bottomRight, WindowState.ACTIVE, lines, selected);
    }

    public ComboBox(int xLeft, int yTop, int width, int height, String[] lines, Integer selected) throws WindowException {
        this(new Point(xLeft, yTop),
                new Point(xLeft + width - 1, yTop + height - 1),
                lines, selected);
    }

    public Integer getSelected() {
        return selected;
    }

    public void setSelected(Integer selected) throws WindowException {
        if ((getLines() == null) && selected != null) throw new WindowException(WindowErrorCode.EMPTY_ARRAY);
        if (selected != null) {
            if (!(selected > (getLines().length - 1) || selected < 0)) {
                this.selected = selected;
            } else throw new WindowException(WindowErrorCode.WRONG_INDEX);
        } else this.selected = null;
    }

    @Override
    public void setLines(String[] lines) {
        super.setLines(lines);
        selected = null;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        ComboBox comboBox = (ComboBox) o;
        return selected.equals(comboBox.selected);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), selected);
    }
}
