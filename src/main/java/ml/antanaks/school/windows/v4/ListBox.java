package ml.antanaks.school.windows.v4;

import ml.antanaks.school.windows.v4.base.RectWindow;
import ml.antanaks.school.windows.v4.base.WindowErrorCode;
import ml.antanaks.school.windows.v4.base.WindowException;
import ml.antanaks.school.windows.v4.base.WindowState;

import java.util.Arrays;

import static ml.antanaks.school.base.StringOperations.isLess;
import static ml.antanaks.school.base.StringOperations.reverse;

public class ListBox extends RectWindow {

    private String[] lines;

    public ListBox(Point topLeft, Point bottomRight, WindowState state, String[] lines) throws WindowException {
        super(topLeft, bottomRight, state);
        if (!(lines == null)) {
            this.lines = new String[lines.length];
            System.arraycopy(lines, 0, this.lines, 0, lines.length);
        }
    }

    public ListBox(Point topLeft, Point bottomRight, String state, String[] lines) throws WindowException {
        this(topLeft, bottomRight, WindowState.fromString(state), lines);
    }

    public ListBox(int xLeft, int yTop, int width, int height, WindowState state, String[] lines) throws WindowException {
        this(new Point(xLeft, yTop),
                new Point(xLeft + width - 1, yTop + height - 1),
                state, lines);
    }

    public ListBox(int xLeft, int yTop, int width, int height, String state, String[] lines) throws WindowException {
        this(new Point(xLeft, yTop),
                new Point(xLeft + width - 1, yTop + height - 1),
                state, lines);
    }

    public ListBox(Point topLeft, Point bottomRight, String[] lines) throws WindowException {
        this(topLeft, bottomRight, WindowState.ACTIVE, lines);
    }

    public ListBox(int xLeft, int yTop, int width, int height, String[] lines) throws WindowException {
        this(xLeft, yTop, width, height, WindowState.ACTIVE, lines);
    }

    public String[] getLines() {
        return lines;
    }

    public void setLines(String[] lines) {
        if (lines == null) {
            this.lines = null;
        } else {
            this.lines = new String[lines.length];
            System.arraycopy(lines, 0, this.lines, 0, lines.length);
        }
    }

    public String[] getLinesSlice(int from, int to) throws WindowException {
        if (lines == null) throw new WindowException(WindowErrorCode.EMPTY_ARRAY);
        if (from < 0 || lines.length < to || from > (to - 1)) throw new WindowException(WindowErrorCode.WRONG_INDEX);
        int endArray = Math.min(lines.length, to);
        String[] strings = new String[endArray - from];
        int startArr = 0;
        for (int i = 0; i < lines.length; i++) {
            if (i >= from && i < endArray) {
                strings[startArr++] = lines[i];
            }
        }
        return strings;
    }

    public String getLine(int index) throws WindowException {
        if (lines == null) {
            throw new WindowException(WindowErrorCode.EMPTY_ARRAY);
        }
        for (int i = 0; i < lines.length; i++) {
            if (i == index) {
                return lines[i];
            }
        }
        throw new WindowException(WindowErrorCode.WRONG_INDEX);
    }

    public void setLine(int index, String line) throws WindowException {
        if (lines == null) {
            throw new WindowException(WindowErrorCode.EMPTY_ARRAY);
        }
        if (index > lines.length - 1 || index < 0) throw new WindowException(WindowErrorCode.WRONG_INDEX);
        for (int i = 0; i < lines.length; i++) {
            if (i == index) {
                lines[i] = line;
            }
        }
    }

    public Integer findLine(String line) {
        if (lines == null) return null;
        for (int i = 0; i < lines.length; i++) {
            if (lines[i].equals(line)) {
                return i;
            }
        }
        return null;
    }

    public void reverseLineOrder() {
        if (!(lines == null)) {
            int size = lines.length;
            int maxIndex = size - 1;
            for (int i = 0; i < size / 2; i++) {
                String temp = lines[i];
                lines[i] = lines[maxIndex - i];
                lines[maxIndex - i] = temp;
            }
        }
    }

    public void reverseLines() throws WindowException {
        if (!(lines == null)) {
            for (int i = 0; i < lines.length; i++) {
                String value = reverse(lines[i]);
                setLine(i, value);
            }
        }
    }

    public void duplicateLines() {
        if (!(lines == null)) {
            String[] strings = new String[2 * lines.length];
            int j = 0;
            for (String line : lines) {
                strings[j++] = line;
                strings[j++] = line;
            }
            setLines(strings);
        }
    }

    public void removeOddLines() {
        if (!(lines == null)) {
            int lengthArr = lines.length % 2 == 0 ? lines.length : lines.length + 1;
            String[] strings = new String[lengthArr / 2];
            int j = 0;
            for (int i = 0; i < lines.length; i++) {
                if (i % 2 == 0) {
                    strings[j++] = lines[i];
                }
            }
            setLines(strings);
        }
    }

    public boolean isSortedDescendant() {
        if (!(lines == null)) {
            for (int i = 0; i < lines.length - 1; i++) {
                if (isLess(lines[i], lines[i + 1])) {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        ListBox listBox = (ListBox) o;
        return Arrays.equals(lines, listBox.lines);
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + Arrays.hashCode(lines);
        return result;
    }
}

