package ml.antanaks.school.windows.v4;

import ml.antanaks.school.windows.v4.base.RectWindow;
import ml.antanaks.school.windows.v4.base.WindowException;
import ml.antanaks.school.windows.v4.base.WindowState;

import java.util.Objects;

public class RectButton extends RectWindow {

    private String text;

    public RectButton(Point topLeft, Point bottomRight, WindowState state, String text) throws WindowException {
        super(topLeft, bottomRight, state);
        this.text = text;
    }

    public RectButton(Point topLeft, Point bottomRight, String state, String text) throws WindowException {
        this(topLeft, bottomRight, WindowState.fromString(state), text);
    }

    public RectButton(int xLeft, int yTop, int width, int height, WindowState state, String text) throws WindowException {
        this(new Point(xLeft, yTop),
                new Point(xLeft + width - 1, yTop + height - 1), state, text);
    }

    public RectButton(int xLeft, int yTop, int width, int height, String state, String text) throws WindowException {
        this(new Point(xLeft, yTop),
                new Point(xLeft + width - 1, yTop + height - 1), state, text);
    }

    public RectButton(Point topLeft, Point bottomRight, String text) throws WindowException {
        this(topLeft, bottomRight, WindowState.ACTIVE, text);
    }

    public RectButton(int xLeft, int yTop, int width, int height, String text) throws WindowException {
        this(xLeft, yTop, width, height, WindowState.ACTIVE, text);
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        RectButton that = (RectButton) o;
        return text.equals(that.text);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), text);
    }
}
