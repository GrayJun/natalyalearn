package ml.antanaks.school.windows.v4;

import ml.antanaks.school.windows.v4.base.WindowException;
import ml.antanaks.school.windows.v4.base.WindowState;

public class WindowFactory {

    private static int countRectButton;
    private static int countRoundButton;

    public static RectButton createRectButton(Point leftTop, Point rightBottom, WindowState state, String text) throws WindowException {
        RectButton rectButton = new RectButton(leftTop, rightBottom, state, text);
        countRectButton++;
        return rectButton;
    }

    public static RoundButton createRoundButton(Point center, int radius, WindowState state, String text) throws WindowException {
        RoundButton roundButton = new RoundButton(center, radius, state, text);
        countRoundButton++;
        return roundButton;
    }

    public static int getRectButtonCount() {
        return countRectButton;
    }

    public static int getRoundButtonCount() {
        return countRoundButton;
    }

    public static int getWindowCount() {
        return countRectButton + countRoundButton;
    }

    public static void reset() {
        countRectButton = 0;
        countRoundButton = 0;
    }
}
