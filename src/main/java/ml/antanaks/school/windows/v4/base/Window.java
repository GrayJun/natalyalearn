package ml.antanaks.school.windows.v4.base;

import ml.antanaks.school.windows.v4.Desktop;
import ml.antanaks.school.windows.v4.Point;
import ml.antanaks.school.windows.v4.iface.Movable;
import ml.antanaks.school.windows.v4.iface.Resizable;

import java.io.Serializable;

import static ml.antanaks.school.windows.v4.base.WindowState.fromString;

public abstract class Window implements Movable, Resizable, Serializable {

    private WindowState state;

    public Window(WindowState state) throws WindowException {
        if (state == null || state == WindowState.DESTROYED) {
            throw new WindowException(WindowErrorCode.WRONG_STATE);
        }
        this.state = state;
    }

    public Window(String stateString) throws WindowException {
        this.state = fromString(stateString);
    }

    public WindowState getState() {
        return state;
    }

    public void setState(WindowState state) throws WindowException {
        if (this.getState() == WindowState.DESTROYED) {
            throw new WindowException(WindowErrorCode.WRONG_STATE);
        }
        this.state = state;
    }

    public abstract boolean isInside(int x, int y);

    public abstract boolean isInside(Point point);

    public abstract boolean isFullyVisibleOnDesktop(Desktop desktop);
}
