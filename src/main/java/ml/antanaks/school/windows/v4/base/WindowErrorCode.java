package ml.antanaks.school.windows.v4.base;

public enum WindowErrorCode {
    WRONG_STATE("Передается WindowState.DESTROYED или null"),
    WRONG_INDEX("Передан недопустимый индекс для массива строк."),
    EMPTY_ARRAY("Массив строк равен null."),
    NULL_WINDOW("Передан null");

    private final String errorString;

    WindowErrorCode(String errorString) {
        this.errorString = errorString;
    }

    public String getErrorString() {
        return errorString;
    }
}
