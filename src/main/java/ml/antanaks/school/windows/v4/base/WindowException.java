package ml.antanaks.school.windows.v4.base;

public class WindowException extends Exception {

    private final WindowErrorCode windowErrorCode;

    public WindowException(WindowErrorCode windowErrorCode) {
        this.windowErrorCode = windowErrorCode;
    }

    public WindowErrorCode getWindowErrorCode() {
        return windowErrorCode;
    }
}
