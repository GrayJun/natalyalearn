package ml.antanaks.school.windows.v4.base;

public enum WindowState {
    ACTIVE, INACTIVE, DESTROYED;

    public static WindowState fromString(String stateString) throws WindowException {
        if (stateString == null || stateString.equals("DESTROYED")) {
            throw new WindowException(WindowErrorCode.WRONG_STATE);
        }
        if (stateString.equals("ACTIVE") || stateString.equals("INACTIVE")) {
            return WindowState.valueOf(stateString);
        } else throw new WindowException(WindowErrorCode.WRONG_STATE);
    }
}