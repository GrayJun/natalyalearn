package ml.antanaks.school.concert;

import com.google.gson.Gson;
import ml.antanaks.school.concert.config.SpringConfig;
import ml.antanaks.school.concert.controller.DataBaseController;
import ml.antanaks.school.concert.controller.SongController;
import ml.antanaks.school.concert.controller.UserController;
import ml.antanaks.school.concert.dto.song.add.AddSongDtoRequest;
import ml.antanaks.school.concert.dto.song.add.AddSongDtoResponse;
import ml.antanaks.school.concert.dto.song.add.SongDto;
import ml.antanaks.school.concert.dto.song.comment.add.AddCommentDtoRequest;
import ml.antanaks.school.concert.dto.song.comment.add.AddCommentDtoResponse;
import ml.antanaks.school.concert.dto.song.comment.edit.EditCommentDtoRequest;
import ml.antanaks.school.concert.dto.song.comment.edit.EditCommentDtoResponse;
import ml.antanaks.school.concert.dto.song.comment.like.add.LikeCommentDtoRequest;
import ml.antanaks.school.concert.dto.song.comment.like.add.LikeCommentDtoResponse;
import ml.antanaks.school.concert.dto.song.comment.like.remove.RemoveLikeCommentDtoRequest;
import ml.antanaks.school.concert.dto.song.comment.like.remove.RemoveLikeCommentDtoResponse;
import ml.antanaks.school.concert.dto.song.comment.remove.RemoveCommentDtoRequest;
import ml.antanaks.school.concert.dto.song.comment.remove.RemoveCommentDtoResponse;
import ml.antanaks.school.concert.dto.song.concert.GetConcertDtoRequest;
import ml.antanaks.school.concert.dto.song.concert.GetConcertDtoResponse;
import ml.antanaks.school.concert.dto.song.get.all.GetSongsDtoRequest;
import ml.antanaks.school.concert.dto.song.get.all.GetSongsDtoResponse;
import ml.antanaks.school.concert.dto.song.get.artist.GetSongsBySongArtistDtoRequest;
import ml.antanaks.school.concert.dto.song.get.artist.GetSongsBySongArtistDtoResponse;
import ml.antanaks.school.concert.dto.song.get.author.GetSongsByAuthorOfWordsDtoRequest;
import ml.antanaks.school.concert.dto.song.get.author.GetSongsByAuthorOfWordsDtoResponse;
import ml.antanaks.school.concert.dto.song.get.composers.GetSongsByComposersDtoRequest;
import ml.antanaks.school.concert.dto.song.get.composers.GetSongsByComposersDtoResponse;
import ml.antanaks.school.concert.dto.song.rating.add.AddRatingDtoRequest;
import ml.antanaks.school.concert.dto.song.rating.add.AddRatingDtoResponse;
import ml.antanaks.school.concert.dto.song.rating.edit.EditRatingDtoRequest;
import ml.antanaks.school.concert.dto.song.rating.edit.EditRatingDtoResponse;
import ml.antanaks.school.concert.dto.song.rating.remove.RemoveRatingDtoRequest;
import ml.antanaks.school.concert.dto.song.rating.remove.RemoveRatingDtoResponse;
import ml.antanaks.school.concert.dto.song.remove.RemoveSongDtoRequest;
import ml.antanaks.school.concert.dto.song.remove.RemoveSongDtoResponse;
import ml.antanaks.school.concert.dto.user.leave.LeaveUserDtoRequest;
import ml.antanaks.school.concert.dto.user.leave.LeaveUserDtoResponse;
import ml.antanaks.school.concert.dto.user.login.LoginUserDtoRequest;
import ml.antanaks.school.concert.dto.user.login.LoginUserDtoResponse;
import ml.antanaks.school.concert.dto.user.logout.LogoutUserDtoRequest;
import ml.antanaks.school.concert.dto.user.logout.LogoutUserDtoResponse;
import ml.antanaks.school.concert.dto.user.register.RegisterUserDtoRequest;
import ml.antanaks.school.concert.dto.user.register.RegisterUserDtoResponse;
import ml.antanaks.school.concert.entity.Comment;
import ml.antanaks.school.concert.entity.Song;
import ml.antanaks.school.concert.entity.User;
import ml.antanaks.school.concert.repository.CommentRepository;
import ml.antanaks.school.concert.repository.SongRepository;
import ml.antanaks.school.concert.repository.UserRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Repeat;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.lang.annotation.Repeatable;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {SpringConfig.class})
@WebAppConfiguration
public class HibernateServerTest {

    @Autowired
    private DataBaseController dataBaseController;

    @Autowired
    private SongController songController;

    @Autowired
    private UserController userController;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private SongRepository songRepository;

    @Autowired
    private CommentRepository commentRepository;

    private String tokenUser1;
    private String tokenUser2;
    private String tokenUser3;
    private String tokenUser4;

    private String songTitle1;
    private String songTitle2;
    private String songTitle3;
    private String songTitle4;

    @Before
    public void before() {
        after();
        addUsersToDataBase();
        createCommunityUser();
        addSongsToDataBase();
        addSongRatingAndCommentsToDataBase();
        addLikesCommentToDataBase();
    }

    @After
    public void after() {
        commentRepository.deleteAll();
        songRepository.deleteAll();
        userRepository.deleteAll();
    }

    private void createCommunityUser() {
        userRepository.save(new User(null, null, "Community", null));
    }

    private void addUsersToDataBase() {
        RegisterUserDtoRequest request1 = new RegisterUserDtoRequest("B1",
                "P1", "login1", "password1");
        String jsonRequestUser1 = new Gson().toJson(request1);
        String jsonResponse1 = userController.registerUser(jsonRequestUser1);
        RegisterUserDtoResponse registerUserDtoResponse1 = new Gson().fromJson(jsonResponse1, RegisterUserDtoResponse.class);
        tokenUser1 = registerUserDtoResponse1.getToken();

        RegisterUserDtoRequest request2 = new RegisterUserDtoRequest("B2",
                "P2", "login2", "password2");
        String jsonRequestUser2 = new Gson().toJson(request2);
        String jsonResponse2 = userController.registerUser(jsonRequestUser2);
        RegisterUserDtoResponse registerUserDtoResponse2 = new Gson().fromJson(jsonResponse2, RegisterUserDtoResponse.class);
        tokenUser2 = registerUserDtoResponse2.getToken();

        RegisterUserDtoRequest request3 = new RegisterUserDtoRequest("B3",
                "P3", "login3", "password3");
        String jsonRequestUser3 = new Gson().toJson(request3);
        String jsonResponse3 = userController.registerUser(jsonRequestUser3);
        RegisterUserDtoResponse registerUserDtoResponse3 = new Gson().fromJson(jsonResponse3, RegisterUserDtoResponse.class);
        tokenUser3 = registerUserDtoResponse3.getToken();

        RegisterUserDtoRequest request4 = new RegisterUserDtoRequest("B4",
                "P4", "login4", "password4");
        String jsonRequestUser4 = new Gson().toJson(request4);
        String jsonResponse4 = userController.registerUser(jsonRequestUser4);
        RegisterUserDtoResponse registerUserDtoResponse4 = new Gson().fromJson(jsonResponse4, RegisterUserDtoResponse.class);
        tokenUser4 = registerUserDtoResponse4.getToken();

        LogoutUserDtoRequest request = new LogoutUserDtoRequest(tokenUser3);
        userController.logoutUser(new Gson().toJson(request));
        if (userRepository.findUserByLogin("login3").getToken() == null) {
            tokenUser3 = null;
        }
    }

    private void addSongsToDataBase() {
        songTitle1 = "Песня1";
        Set<String> composer = Stream.of("Композитор11", "Композитор12", "Композитор13").collect(Collectors.toSet());
        Set<String> authorOfWords = Stream.of("Автор11", "Автор12").collect(Collectors.toSet());
        String songArtist = "Исполнитель1";
        String songTiming = "150";

        List<SongDto> songsUser1 = Stream.of(new SongDto(songTitle1, composer, authorOfWords, songArtist, songTiming))
                .collect(Collectors.toList());
        AddSongDtoRequest addSongDtoRequest = new AddSongDtoRequest(songsUser1, tokenUser1);
        String jsonAddSong = new Gson().toJson(addSongDtoRequest);
        songController.addSong(jsonAddSong);

        songTitle2 = "Песня2";
        Set<String> composer2 = Stream.of("Композитор21", "Композитор22", "Композитор23").collect(Collectors.toSet());
        Set<String> authorOfWords2 = Stream.of("Автор21", "Автор22").collect(Collectors.toSet());
        String songArtist2 = "Исполнитель2";
        String songTiming2 = "300";

        List<SongDto> songsUser2 = Stream.of(new SongDto(songTitle2, composer2, authorOfWords2, songArtist2, songTiming2))
                .collect(Collectors.toList());
        AddSongDtoRequest addSongDtoRequest2 = new AddSongDtoRequest(songsUser2, tokenUser2);
        String jsonAddSong2 = new Gson().toJson(addSongDtoRequest2);
        songController.addSong(jsonAddSong2);

        songTitle3 = "Песня3";
        Set<String> composer3 = Stream.of("Композитор31", "Композитор32").collect(Collectors.toSet());
        Set<String> authorOfWords3 = Stream.of("Автор31").collect(Collectors.toSet());
        String songArtist3 = "Исполнитель3";
        String songTiming3 = "3500";

        List<SongDto> songsUser3 = Stream.of(new SongDto(songTitle3, composer3, authorOfWords3, songArtist3, songTiming3))
                .collect(Collectors.toList());

        AddSongDtoRequest addSongDtoRequest3 = new AddSongDtoRequest(songsUser3, tokenUser2);
        String jsonAddSong3 = new Gson().toJson(addSongDtoRequest3);
        songController.addSong(jsonAddSong3);

        songTitle4 = "Песня4";
        Set<String> composer4 = Stream.of("Композитор31", "Композитор32").collect(Collectors.toSet());
        Set<String> authorOfWords4 = Stream.of("Автор41").collect(Collectors.toSet());
        String songArtist4 = "Исполнитель4";
        String songTiming4 = "55";

        List<SongDto> songsUser4 = Stream.of(new SongDto(songTitle4, composer4, authorOfWords4, songArtist4, songTiming4))
                .collect(Collectors.toList());

        AddSongDtoRequest addSongDtoRequest4 = new AddSongDtoRequest(songsUser4, tokenUser4);
        String jsonAddSong4 = new Gson().toJson(addSongDtoRequest4);
        songController.addSong(jsonAddSong4);
    }

    private void addSongRatingAndCommentsToDataBase() {
        Song song1 = songRepository.findSongBySongTitle(songTitle1);
        Map<String, Integer> rating1 = song1.getRating();
        rating1.put("login2", 2);
        rating1.put("login3", 5);
        song1.getComments().add(new Comment("Хорошая1", "login2"));
        song1.getComments().add(new Comment("Клевая1", "login3"));

        Song song2 = songRepository.findSongBySongTitle(songTitle2);
        Map<String, Integer> rating2 = song2.getRating();
        rating2.put("login1", 4);
        rating2.put("login3", 1);
        song2.getComments().add(new Comment("Хорошая2", "login2"));
        song2.getComments().add(new Comment("Клевая2", "login3"));

        Song song4 = songRepository.findSongBySongTitle(songTitle4);
        song4.getComments().add(new Comment("Клевая4", "login1"));

        songRepository.save(song1);
        songRepository.save(song2);
        songRepository.save(song4);

        Song song3 = songRepository.findSongBySongTitle(songTitle3);
        song3.getComments().add(new Comment("Клевая3", "login2"));
        songRepository.save(song3);
    }

    private void addLikesCommentToDataBase() {
        Song song1 = songRepository.findSongBySongTitle(songTitle1);
        Comment comment1 = commentRepository.findCommentBySongTitleAndCreatorLogin(song1.getSongTitle(), "login2");
        comment1.getLikes().add("login3");
        comment1.getLikes().add("login4");

        Song song2 = songRepository.findSongBySongTitle(songTitle2);
        Comment comment21 = commentRepository.findCommentBySongTitleAndCreatorLogin(song2.getSongTitle(), "login2");
        comment21.getLikes().add("login1");
        comment21.getLikes().add("login3");
        Comment comment22 = commentRepository.findCommentBySongTitleAndCreatorLogin(song2.getSongTitle(), "login3");
        comment22.getLikes().add("login2");

        Song song4 = songRepository.findSongBySongTitle(songTitle4);
        Comment comment4 = commentRepository.findCommentBySongTitleAndCreatorLogin(song4.getSongTitle(), "login1");
        comment4.getLikes().add("login2");
        comment4.getLikes().add("login3");

        commentRepository.save(comment1);
        commentRepository.save(comment21);
        commentRepository.save(comment22);
        commentRepository.save(comment4);
    }

    @Test
    public void testRegisterUserShouldSuccess() {
        String login = "login";
        RegisterUserDtoRequest request = new RegisterUserDtoRequest("Brad",
                "Pitt", login, "ppassword");
        String jsonRequst = new Gson().toJson(request);
        String jsonResponse = userController.registerUser(jsonRequst);
        assertTrue(jsonResponse.contains("token"));
        RegisterUserDtoResponse response = new Gson().fromJson(jsonResponse, RegisterUserDtoResponse.class);

        assertNotNull(response.getToken());
        User userByLogin = userRepository.findUserByLogin(login);
        assertNotNull(userByLogin);
        assertEquals("Brad", userByLogin.getFirstName());
        assertEquals("Pitt", userByLogin.getLastName());
        assertEquals(response.getToken(), userByLogin.getToken().toString());
    }

    @Test
    public void testRegisterUserShouldWrong1() {
        String login = "login";
        RegisterUserDtoRequest requestUser1 = new RegisterUserDtoRequest("B1",
                "P1", login, "password1");
        RegisterUserDtoRequest requestUser2 = new RegisterUserDtoRequest("B2",
                "P2", login, "password2");
        String jsonRequst1 = new Gson().toJson(requestUser1);
        String jsonRequst2 = new Gson().toJson(requestUser2);
        String jsonResponse1 = userController.registerUser(jsonRequst1);
        String jsonResponse2 = userController.registerUser(jsonRequst2);
        assertTrue(jsonResponse1.contains("token"));
        assertTrue(jsonResponse2.contains("Логин уже существует"));
        assertFalse(jsonResponse2.contains("token"));
        RegisterUserDtoResponse response1 = new Gson().fromJson(jsonResponse1, RegisterUserDtoResponse.class);
        RegisterUserDtoResponse response2 = new Gson().fromJson(jsonResponse2, RegisterUserDtoResponse.class);

        assertNotNull(response1.getToken());
        assertNull(response2.getToken());

        User userByLogin1 = userRepository.findUserByLogin(login);
        if (userByLogin1 != null) {
            assertEquals("B1", userByLogin1.getFirstName());
            assertEquals(response1.getToken(), userByLogin1.getToken().toString());
        }
    }

    @Test
    public void testRegisterUserShouldWrong2() {
        String login1 = "login13";
        String login2 = "login23";
        RegisterUserDtoRequest requestUser1 = new RegisterUserDtoRequest("",
                "Pitt1", login1, "pit1963t19631");
        RegisterUserDtoRequest requestUser2 = new RegisterUserDtoRequest("Brad2",
                "Pitt2", login2, "pit1963t19632");
        String jsonRequst1 = new Gson().toJson(requestUser1);
        String jsonRequst2 = new Gson().toJson(requestUser2);
        String jsonResponse1 = userController.registerUser(jsonRequst1);
        String jsonResponse2 = userController.registerUser(jsonRequst2);
        assertTrue(jsonResponse1.contains("SERVER_USER_WRONG_FIRSTNAME"));
        assertTrue(jsonResponse2.contains("token"));
    }

    @Test
    public void testLogoutUserShouldSuccess() {
        User userByToken = userRepository.findUserByToken(tokenUser1);
        LogoutUserDtoRequest request = new LogoutUserDtoRequest(tokenUser1);
        String response = userController.logoutUser(new Gson().toJson(request));
        assertNull(userRepository.findUserByLogin(userByToken.getLogin()).getToken());
        LogoutUserDtoResponse logoutUserDtoResponse = new Gson().fromJson(response, LogoutUserDtoResponse.class);
        assertEquals("Пользователь упешно вышел", logoutUserDtoResponse.getMessage());
    }

    @Test
    public void testLogoutUserShouldWrong1() {
        LogoutUserDtoRequest request = new LogoutUserDtoRequest(UUID.randomUUID().toString());
        String response = userController.logoutUser(new Gson().toJson(request));
        LogoutUserDtoResponse logoutUserDtoResponse = new Gson().fromJson(response, LogoutUserDtoResponse.class);
        assertNotEquals("Пользователь упешно вышел", logoutUserDtoResponse.getMessage());
        assertEquals("DATABASE_NOT_FOUND", logoutUserDtoResponse.getError());
        assertEquals("Логин или токен в базе не найден", logoutUserDtoResponse.getErrorMessage());
    }

    @Test
    public void testLoginUserShouldSuccess() {
        String login = "login3";
        LoginUserDtoRequest request = new LoginUserDtoRequest(login, "password3");
        String requestJson = new Gson().toJson(request);
        String response = userController.loginUser(requestJson);
        LoginUserDtoResponse loginUserDtoResponse = new Gson().fromJson(response, LoginUserDtoResponse.class);
        assertNotNull(loginUserDtoResponse.getToken());
    }

    @Test
    public void testLoginUserShouldWrong() {
        String login = "login3";
        LoginUserDtoRequest request = new LoginUserDtoRequest(login, "password3Wrong");
        String requestJson = new Gson().toJson(request);
        String response = userController.loginUser(requestJson);
        LoginUserDtoResponse loginUserDtoResponse = new Gson().fromJson(response, LoginUserDtoResponse.class);
        assertFalse(loginUserDtoResponse.getError().isEmpty());
    }

    @Test
    public void testAddSongsShouldSuccess() {
        String songTitle = "Песня";
        Set<String> composer = Stream.of("Композитор1", "Композитор2", "Композитор3").collect(Collectors.toSet());
        Set<String> authorOfWords = Stream.of("Автор1", "Автор2").collect(Collectors.toSet());
        String songArtist = "Исполнитель";
        String songTiming = "16";

        List<SongDto> collectSongs = Stream.of(new SongDto(songTitle, composer, authorOfWords, songArtist, songTiming))
                .collect(Collectors.toList());

        AddSongDtoRequest addSongDtoRequest = new AddSongDtoRequest(collectSongs, tokenUser1);
        String jsonAddSong = new Gson().toJson(addSongDtoRequest);
        String response = songController.addSong(jsonAddSong);
        AddSongDtoResponse addSongDtoResponse = new Gson().fromJson(response, AddSongDtoResponse.class);
        assertEquals("Успешно добавлено в программу концерта", addSongDtoResponse.getMessage());

        Song song = songRepository.findSongBySongTitle(songTitle);
        assertEquals(songTitle, song.getSongTitle());
        assertEquals(composer, song.getComposer());
        assertEquals(authorOfWords, song.getAuthorOfWords());
        assertEquals(songArtist, song.getSongArtist());
        assertEquals(Integer.parseInt(songTiming), song.getSongTiming());
        String login = userRepository.findUserByToken(tokenUser1).getLogin();
        assertEquals(login, song.getUser().getLogin());
        assertEquals(Integer.valueOf(5), song.getRating().get(login));
    }

    @Test
    public void testAddSongsShouldWrong() {
        String songTitle = "Песня1";
        Set<String> composer = Stream.of("Композитор11", "Композитор12", "Композитор13").collect(Collectors.toSet());
        Set<String> authorOfWords = Stream.of("Автор11", "Автор12").collect(Collectors.toSet());
        String songArtist = "Исполнитель1";
        String songTiming = "15";

        List<SongDto> collectSongs = Stream.of(new SongDto(songTitle, composer, authorOfWords, songArtist, songTiming))
                .collect(Collectors.toList());

        AddSongDtoRequest addSongDtoRequest = new AddSongDtoRequest(collectSongs, UUID.randomUUID().toString());
        String jsonAddSong = new Gson().toJson(addSongDtoRequest);
        String response = songController.addSong(jsonAddSong);
        AddSongDtoResponse addSongDtoResponse = new Gson().fromJson(response, AddSongDtoResponse.class);
        assertNotEquals("Пользователь упешно вышел", addSongDtoResponse.getMessage());
        assertEquals("DATABASE_NOT_FOUND", addSongDtoResponse.getError());
        assertEquals("Логин или токен в базе не найден", addSongDtoResponse.getErrorMessage());
    }

    @Test
    public void testRemoveSongsShouldSuccess() {
        RemoveSongDtoRequest removeSongDtoRequest = new RemoveSongDtoRequest("Песня3", tokenUser2);
        RemoveSongDtoResponse response = songController.removeSong(removeSongDtoRequest);
        assertEquals("Успешно удалена", response.getMessage());
        assertNull(songRepository.findSongBySongTitle("Песня3"));
    }

    @Test
    public void testRemoveSongsAndChangeUserShouldSuccess() {
        RemoveSongDtoRequest removeSongDtoRequest = new RemoveSongDtoRequest("Песня1", tokenUser1);
        RemoveSongDtoResponse response = songController.removeSong(removeSongDtoRequest);
        assertEquals("Успешно удалена", response.getMessage());
        assertNotNull(songRepository.findSongBySongTitle("Песня1"));
        assertEquals("Community", songRepository.findSongBySongTitle("Песня1").getUser().getLogin());
    }

    @Test
    public void testRemoveSongsShouldWrong() {
        RemoveSongDtoRequest removeSongDtoRequest = new RemoveSongDtoRequest("Песня1", tokenUser2);
        RemoveSongDtoResponse response = songController.removeSong(removeSongDtoRequest);
        assertEquals("DATABASE_WRONG_REMOVE_SONG", response.getError());
        assertEquals("Песня1", songRepository.findSongBySongTitle("Песня1").getSongTitle());
    }

    @Test
    public void testAddRatingShouldSuccess() {
        AddRatingDtoRequest addRatingDtoRequest = new AddRatingDtoRequest(songTitle1, 4, tokenUser4);
        String json = new Gson().toJson(addRatingDtoRequest);
        String response = songController.addRating(json);
        AddRatingDtoResponse removeSongDtoResponse = new Gson().fromJson(response, AddRatingDtoResponse.class);
        assertEquals("Успешно добавлено", removeSongDtoResponse.getMessage());
        Integer integer = songRepository.findSongBySongTitle(songTitle1).getRating().get("login4");
        assertEquals(Integer.valueOf(4), integer);
    }

    @Test
    public void testAddRatingShouldWrong() {
        AddRatingDtoRequest addRatingDtoRequest = new AddRatingDtoRequest(songTitle1, 3, tokenUser1);
        String json = new Gson().toJson(addRatingDtoRequest);
        String response = songController.addRating(json);
        AddRatingDtoResponse removeSongDtoResponse = new Gson().fromJson(response, AddRatingDtoResponse.class);
        assertEquals("DATABASE_WRONG_EDIT_RATING", removeSongDtoResponse.getError());
    }

    @Test
    public void testEditRatingShouldSuccess() {
        EditRatingDtoRequest editRatingDtoRequest = new EditRatingDtoRequest(songTitle1, 5, tokenUser2);
        String json = new Gson().toJson(editRatingDtoRequest);
        Integer integer = songRepository.findSongBySongTitle(songTitle1).getRating().get("login2");
        assertEquals(Integer.valueOf(2), integer);
        String response = songController.editRating(json);
        EditRatingDtoResponse editRatingDtoResponse = new Gson().fromJson(response, EditRatingDtoResponse.class);
        assertEquals("Успешно добавлено", editRatingDtoResponse.getMessage());
        Integer newInteger = songRepository.findSongBySongTitle(songTitle1).getRating().get("login2");
        assertEquals(Integer.valueOf(5), newInteger);
    }

    @Test
    public void testEditRatingShouldWrong() {
        EditRatingDtoRequest editRatingDtoRequest = new EditRatingDtoRequest(songTitle1, 3, tokenUser1);
        String json = new Gson().toJson(editRatingDtoRequest);
        Integer integer = songRepository.findSongBySongTitle(songTitle1).getRating().get("login1");
        assertEquals(Integer.valueOf(5), integer);
        String response = songController.editRating(json);
        EditRatingDtoResponse editRatingDtoResponse = new Gson().fromJson(response, EditRatingDtoResponse.class);
        assertEquals("DATABASE_WRONG_EDIT_RATING", editRatingDtoResponse.getError());
        Integer newInteger = songRepository.findSongBySongTitle(songTitle1).getRating().get("login1");
        assertEquals(Integer.valueOf(5), newInteger);
    }

    @Test
    public void testRemoveRatingShouldSuccess() {
        RemoveRatingDtoRequest dtoRequest = new RemoveRatingDtoRequest(songTitle1, tokenUser2);
        String json = new Gson().toJson(dtoRequest);
        Integer integer = songRepository.findSongBySongTitle(songTitle1).getRating().get("login2");
        assertEquals(Integer.valueOf(2), integer);
        String response = songController.removeRating(json);
        RemoveRatingDtoResponse dtoResponse = new Gson().fromJson(response, RemoveRatingDtoResponse.class);
        assertEquals("Успешно удалено", dtoResponse.getMessage());
        assertNull(songRepository.findSongBySongTitle(songTitle1).getRating().get("login2"));
    }

    @Test
    public void testRemoveRatingShouldWrong() {
        RemoveRatingDtoRequest dtoRequest = new RemoveRatingDtoRequest(songTitle2, tokenUser2);
        String json = new Gson().toJson(dtoRequest);
        Integer integer = songRepository.findSongBySongTitle(songTitle2).getRating().get("login2");
        assertEquals(Integer.valueOf(5), integer);
        String response = songController.removeRating(json);
        RemoveRatingDtoResponse dtoResponse = new Gson().fromJson(response, RemoveRatingDtoResponse.class);
        assertEquals("DATABASE_WRONG_REMOVE_RATING", dtoResponse.getError());
    }

    @Test
    public void testAddCommentShouldSuccess() {
        String text = "Хорошая";
        AddCommentDtoRequest dtoRequest = new AddCommentDtoRequest(songTitle4, text, tokenUser4);
        String json = new Gson().toJson(dtoRequest);
        String response = songController.addComment(json);
        AddCommentDtoResponse dtoResponse = new Gson().fromJson(response, AddCommentDtoResponse.class);
        assertEquals("Успешно добавлен", dtoResponse.getMessage());
        Comment comment = commentRepository.findCommentBySongTitleAndCreatorLogin(songTitle4, "login4");
        assertEquals("Хорошая", comment.getText());
    }

    @Test
    public void testAddCommentShouldWrong() {
        String text = "Плохая";
        AddCommentDtoRequest dtoRequest = new AddCommentDtoRequest(songTitle1, text, tokenUser2);
        String json = new Gson().toJson(dtoRequest);
        String response = songController.addComment(json);
        AddCommentDtoResponse dtoResponse = new Gson().fromJson(response, AddCommentDtoResponse.class);
        assertEquals("DATABASE_WRONG_EDIT_COMMENT", dtoResponse.getError());
        Comment comment = commentRepository.findCommentBySongTitleAndCreatorLogin(songTitle1, "login2");
        assertNotEquals(text, comment.getText());
    }

    @Test
    public void testEditCommentShouldSuccess() {
        EditCommentDtoRequest dtoRequest = new EditCommentDtoRequest(songTitle1, "Ну такое1", tokenUser2);
        String json = new Gson().toJson(dtoRequest);
        Comment comment = commentRepository.findCommentBySongTitleAndCreatorLogin(songTitle1, "login2");
        assertEquals("Хорошая1", comment.getText());
        String response = songController.editComment(json);
        EditCommentDtoResponse dtoResponse = new Gson().fromJson(response, EditCommentDtoResponse.class);
        assertEquals("Успешно изменение добавлено", dtoResponse.getMessage());
        Comment newComment = commentRepository.findCommentBySongTitleAndCreatorLogin(songTitle1, "login2");
        assertEquals("Ну такое1", newComment.getText());
        Comment oldComment = commentRepository.findCommentBySongTitleAndCreatorLogin(songTitle1, "Community");
        assertEquals("Хорошая1", oldComment.getText());
        assertEquals("Community", oldComment.getCreatorLogin());
    }

    @Test
    public void testEditCommentShouldSuccess2() {
        EditCommentDtoRequest dtoRequest = new EditCommentDtoRequest(songTitle3, "НОВАЯ Клевая3", tokenUser2);
        String json = new Gson().toJson(dtoRequest);
        Comment comment = commentRepository.findCommentBySongTitleAndCreatorLogin(songTitle3, "login2");
        assertEquals("Клевая3", comment.getText());
        String response = songController.editComment(json);
        EditCommentDtoResponse dtoResponse = new Gson().fromJson(response, EditCommentDtoResponse.class);
        assertEquals("Успешно изменение добавлено", dtoResponse.getMessage());
        Comment newComment = commentRepository.findCommentBySongTitleAndCreatorLogin(songTitle3, "login2");
        assertEquals("НОВАЯ Клевая3", newComment.getText());
        Comment oldComment = commentRepository.findCommentBySongTitleAndCreatorLogin(songTitle3, "Community");
        assertNull(oldComment);
    }

    @Test
    public void testEditCommentShouldWrong() {
        EditCommentDtoRequest dtoRequest = new EditCommentDtoRequest(songTitle1, "Ну такое1", tokenUser4);
        String json = new Gson().toJson(dtoRequest);
        assertNull(commentRepository.findCommentBySongTitleAndCreatorLogin(songTitle1, "login4"));
        String response = songController.editComment(json);
        EditCommentDtoResponse dtoResponse = new Gson().fromJson(response, EditCommentDtoResponse.class);
        assertEquals("DATABASE_COMMENT_NOT_FOUND", dtoResponse.getError());
    }

    @Test
    public void testRemoveCommentShouldSuccess() {
        RemoveCommentDtoRequest dtoRequest = new RemoveCommentDtoRequest(songTitle2, tokenUser2);
        RemoveCommentDtoResponse dtoResponse = songController.removeComment(dtoRequest);
        assertEquals("Успешно добавлено", dtoResponse.getMessage());
        assertNull(commentRepository.findCommentBySongTitleAndCreatorLogin(songTitle2, "login2"));
        Comment comment = commentRepository.findCommentBySongTitleAndCreatorLogin(songTitle2, "Community");
        assertEquals("Community", comment.getCreatorLogin());
    }

    @Test
    public void testRemoveCommentShouldWrong() {
        RemoveCommentDtoRequest dtoRequest = new RemoveCommentDtoRequest(songTitle2, tokenUser4);
        RemoveCommentDtoResponse dtoResponse = songController.removeComment(dtoRequest);
        assertEquals("DATABASE_COMMENT_NOT_FOUND", dtoResponse.getError());
    }

    @Test
    public void testLikeCommentShouldSuccess() {
        LikeCommentDtoRequest dtoRequest = new LikeCommentDtoRequest(songTitle2, "login3", tokenUser4);
        String json = new Gson().toJson(dtoRequest);
        Comment comment = commentRepository.findCommentBySongTitleAndCreatorLogin(songTitle2, "login3");
        Set<String> likes = comment.getLikes();
        assertFalse(likes.contains("login4"));
        String response = songController.likeComment(json);
        LikeCommentDtoResponse dtoResponse = new Gson().fromJson(response, LikeCommentDtoResponse.class);
        assertEquals("Успешно добавлен", dtoResponse.getMessage());
        Comment commentWithNewLike = commentRepository.findCommentBySongTitleAndCreatorLogin(songTitle2, "login3");
        Set<String> newLikes = commentWithNewLike.getLikes();
        assertTrue(newLikes.contains("login4"));
    }

    @Test
    public void testLikeCommentShouldWrong() {
        LikeCommentDtoRequest dtoRequest = new LikeCommentDtoRequest(songTitle2, "login1", tokenUser4);
        String json = new Gson().toJson(dtoRequest);
        String response = songController.likeComment(json);
        LikeCommentDtoResponse dtoResponse = new Gson().fromJson(response, LikeCommentDtoResponse.class);
        assertEquals("DATABASE_COMMENT_NOT_FOUND", dtoResponse.getError());
    }

    @Test
    public void testRemoveLikeCommentShouldSuccess() {
        assertTrue(commentRepository.findCommentBySongTitleAndCreatorLogin(songTitle2, "login2").getLikes().contains("login1"));
        RemoveLikeCommentDtoRequest dtoRequest = new RemoveLikeCommentDtoRequest(songTitle2, "login2", tokenUser1);
        String json = new Gson().toJson(dtoRequest);
        String response = songController.removeLikeComment(json);
        RemoveLikeCommentDtoResponse dtoResponse = new Gson().fromJson(response, RemoveLikeCommentDtoResponse.class);
        assertEquals("Изменение успешно добавлено", dtoResponse.getMessage());
        assertFalse(commentRepository.findCommentBySongTitleAndCreatorLogin(songTitle2, "login2").getLikes().contains("login1"));
    }

    @Test
    public void testRemoveLikeCommentShouldWrong() {
        RemoveLikeCommentDtoRequest dtoRequest = new RemoveLikeCommentDtoRequest(songTitle2, "login2", tokenUser4);
        String json = new Gson().toJson(dtoRequest);
        String response = songController.removeLikeComment(json);
        RemoveLikeCommentDtoResponse dtoResponse = new Gson().fromJson(response, RemoveLikeCommentDtoResponse.class);
        assertEquals("DATABASE_WRONG_EDIT_LIKE", dtoResponse.getError());
    }

    @Test
    public void testGetSongsShouldSuccess() {
        GetSongsDtoRequest request = new GetSongsDtoRequest(tokenUser4);
        String requestJson = new Gson().toJson(request);
        String response = songController.getSongs(requestJson);
        GetSongsDtoResponse dtoResponse = new Gson().fromJson(response, GetSongsDtoResponse.class);
        List<Song> songsResponse = dtoResponse.getSongs();
        assertEquals(4, songsResponse.size());
        assertTrue(response.contains(songTitle1));
        assertTrue(response.contains(songTitle2));
        assertTrue(response.contains(songTitle3));
        assertTrue(response.contains(songTitle4));
    }

    @Test
    public void testGetSongsShouldWrong() {
        after();
        GetSongsDtoRequest request = new GetSongsDtoRequest(tokenUser2);
        String requestJson = new Gson().toJson(request);
        String response = songController.getSongs(requestJson);
        GetSongsDtoResponse dtoResponse = new Gson().fromJson(response, GetSongsDtoResponse.class);
        assertEquals("DATABASE_WRONG_POOL_SONG", dtoResponse.getError());
    }

    @Test
    public void testGetSongsByComposersShouldSuccess() {
        Set<String> collect = Stream.of("Композитор31", "Композитор32").collect(Collectors.toSet());
        GetSongsByComposersDtoRequest request = new GetSongsByComposersDtoRequest(collect, tokenUser4);
        String requestJson = new Gson().toJson(request);
        String response = songController.getSongsByComposers(requestJson);
        GetSongsByComposersDtoResponse dtoResponse = new Gson().fromJson(response, GetSongsByComposersDtoResponse.class);
        List<Song> songsResponse = dtoResponse.getSongs();
        assertEquals(2, songsResponse.size());
        assertTrue(response.contains(songTitle3));
        assertTrue(response.contains(songTitle4));
    }

    @Test
    public void testGetSongsByComposersShouldWrong() {
        after();
        Set<String> collect = Stream.of("Композитор31", "Композитор32").collect(Collectors.toSet());
        GetSongsByComposersDtoRequest request = new GetSongsByComposersDtoRequest(collect, tokenUser4);
        String requestJson = new Gson().toJson(request);
        String response = songController.getSongsByComposers(requestJson);
        GetSongsByComposersDtoResponse dtoResponse = new Gson().fromJson(response, GetSongsByComposersDtoResponse.class);
        assertEquals("DATABASE_WRONG_POOL_SONG", dtoResponse.getError());
    }

    @Test
    public void testGetSongsByAuthorOfWordsShouldSuccess() {
        Set<String> collect = Stream.of("Автор31").collect(Collectors.toSet());
        GetSongsByAuthorOfWordsDtoRequest request = new GetSongsByAuthorOfWordsDtoRequest(collect, tokenUser1);
        String requestJson = new Gson().toJson(request);
        String response = songController.getSongsByAuthorOfWords(requestJson);
        GetSongsByAuthorOfWordsDtoResponse dtoResponse = new Gson().fromJson(response, GetSongsByAuthorOfWordsDtoResponse.class);
        List<Song> songsResponse = dtoResponse.getSongs();
        assertEquals(1, songsResponse.size());
        assertTrue(response.contains(songTitle3));
    }

    @Test
    public void testGetSongsByAuthorOfWordsShouldWrong() {
        after();
        Set<String> collect = Stream.of("Автор31").collect(Collectors.toSet());
        GetSongsByAuthorOfWordsDtoRequest request = new GetSongsByAuthorOfWordsDtoRequest(collect, tokenUser1);
        String requestJson = new Gson().toJson(request);
        String response = songController.getSongsByAuthorOfWords(requestJson);
        GetSongsBySongArtistDtoResponse dtoResponse = new Gson().fromJson(response, GetSongsBySongArtistDtoResponse.class);
        assertEquals("DATABASE_WRONG_POOL_SONG", dtoResponse.getError());
    }

    @Test
    public void testSongsBySongArtistShouldSuccess() {
        GetSongsBySongArtistDtoRequest request = new GetSongsBySongArtistDtoRequest("Исполнитель1", tokenUser1);
        String requestJson = new Gson().toJson(request);
        String response = songController.getSongsBySongArtist(requestJson);
        GetSongsBySongArtistDtoResponse dtoResponse = new Gson().fromJson(response, GetSongsBySongArtistDtoResponse.class);
        List<Song> songsResponse = dtoResponse.getSongs();
        assertEquals(1, songsResponse.size());
        assertTrue(response.contains(songTitle1));
    }

    @Test
    public void testSongsBySongArtistShouldWrong() {
        after();
        GetSongsBySongArtistDtoRequest request = new GetSongsBySongArtistDtoRequest("Исполнитель1", tokenUser1);
        String requestJson = new Gson().toJson(request);
        String response = songController.getSongsBySongArtist(requestJson);
        GetSongsBySongArtistDtoResponse dtoResponse = new Gson().fromJson(response, GetSongsBySongArtistDtoResponse.class);
        assertEquals("DATABASE_WRONG_POOL_SONG", dtoResponse.getError());
    }

    @Test
    public void testGetConcertShouldSuccess() {
        GetConcertDtoRequest request = new GetConcertDtoRequest(tokenUser4);
        String requestJson = new Gson().toJson(request);
        String response = songController.getConcert(requestJson);
        GetConcertDtoResponse dtoResponse = new Gson().fromJson(response, GetConcertDtoResponse.class);
        assertEquals("Песня1", dtoResponse.getConcert().get(0).getSongTitle());
        assertEquals(4.0, dtoResponse.getConcert().get(0).getAverageRating(), 0);
        assertEquals("Песня2", dtoResponse.getConcert().get(1).getSongTitle());
        assertEquals(3.3, dtoResponse.getConcert().get(1).getAverageRating(), 0);
        assertEquals("Песня4", dtoResponse.getConcert().get(2).getSongTitle());
    }

    @Test
    public void testGetConcertShouldWrong() {
        after();
        GetConcertDtoRequest request = new GetConcertDtoRequest(tokenUser4);
        String requestJson = new Gson().toJson(request);
        String response = songController.getConcert(requestJson);
        GetConcertDtoResponse dtoResponse = new Gson().fromJson(response, GetConcertDtoResponse.class);
        assertEquals("DATABASE_WRONG_POOL_SONG", dtoResponse.getError());
    }

    /**
     * Как должен отработать лив:
     * 1. Удалится первый юзер из всех юзеров.
     * 2. В первой песне поменяется createrLogin на сообщество
     * 3. В песню четыре добавить коммент от первого и пролайкать несколькими чуваками коммент
     * и этот коммент при лив станет от комьюнити.
     */
    @Test
    public void testLeaveUserShouldSuccess() {
        User userByToken = userRepository.findUserByToken(tokenUser1);
        assertEquals(userByToken.getLogin(), songRepository.findSongBySongTitle(songTitle1).getUser().getLogin());
        LeaveUserDtoRequest request = new LeaveUserDtoRequest(tokenUser1);
        String requestJson = new Gson().toJson(request);
        String response = userController.leaveUser(requestJson);
        LeaveUserDtoResponse userDtoResponse = new Gson().fromJson(response, LeaveUserDtoResponse.class);
        assertEquals("Пользователь упешно покинул сервер", userDtoResponse.getMessage());
        assertNull(userRepository.findUserByLogin(userByToken.getLogin()));
        assertEquals("Community",
                songRepository.findSongBySongTitle(songTitle1).getUser().getLogin());
        assertEquals("Клевая4",
                commentRepository.findCommentBySongTitleAndCreatorLogin(songTitle4, "Community").getText());
    }

    @Test
    public void testLeaveUserShouldSuccess2() {
        LeaveUserDtoRequest request = new LeaveUserDtoRequest(tokenUser4);
        String requestJson = new Gson().toJson(request);
        String response = userController.leaveUser(requestJson);
        LeaveUserDtoResponse dtoResponse = new Gson().fromJson(response, LeaveUserDtoResponse.class);
        assertEquals("Пользователь упешно покинул сервер", dtoResponse.getMessage());
        assertNull(songRepository.findSongBySongTitle(songTitle4));
    }

    @Test
    public void testLeaveUserShouldWrong() {
        LeaveUserDtoRequest request = new LeaveUserDtoRequest(UUID.randomUUID().toString());
        String response = userController.leaveUser(new Gson().toJson(request));
        LeaveUserDtoResponse userDtoResponse = new Gson().fromJson(response, LeaveUserDtoResponse.class);
        assertNotEquals("Пользователь упешно вышел", userDtoResponse.getMessage());
        assertEquals("DATABASE_NOT_FOUND", userDtoResponse.getError());
        assertEquals("Логин или токен в базе не найден", userDtoResponse.getErrorMessage());
    }
}
